package cz.mendelu.wifilocalizator;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PowerManager.WakeLock;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.qozix.mapview.MapView;

import cz.mendelu.wifilocalizator.async.AsyncTrainNeuralNetwork;
import cz.mendelu.wifilocalizator.dataholers.ScanResultsFloors;
import cz.mendelu.wifilocalizator.interfaces.MapUpdated;
import cz.mendelu.wifilocalizator.managers.SettingsManager;
import cz.mendelu.wifilocalizator.other.Constants;
import cz.mendelu.wifilocalizator.other.ManageXmlData;
import cz.mendelu.wifilocalizator.other.MapHelper;
import cz.mendelu.wifilocalizator.other.Static;

public class DefaultMapActivity extends ActionBarActivity {
     WakeLock wl;
     ScanResultsFloors scanResultsFloors;
     SettingsManager settingsManager;
    
    // child activity
    private static ActionBarActivity childActivity;
    private static int childActivityType = Constants.NO_ACTIVITY;
    
    
    // Drawer
	private DrawerLayout			mDrawerLayout;
	private ListView				mDrawerList;
	private ActionBarDrawerToggle	mDrawerToggle;

	private CharSequence			mDrawerTitle;
	private CharSequence			mTitle;
	private String[]				mFloorsTitles;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// set empty_activity
		setContentView( R.layout.drawer_activity );
		// setup Activity
		Static.hideTitleBar(this);
		Static.enableWifi(this);
		wl = Static.takeWakeLock(this);
		
		// create settings manager
		SettingsManager.init(this);
		
		// create scanResultsFloors
		scanResultsFloors = new ScanResultsFloors(getApplicationContext());
		
		// load XML file
		int numOfLoadedFloors = ManageXmlData.load(scanResultsFloors); // wifi_data.xml
		Static.showToast(this, getString(R.string.loaded_data_for_floors_1)+" "+numOfLoadedFloors+" "+getString(R.string.loaded_data_for_floors_2));
		
		if (! scanResultsFloors.isGridSizeOk(SettingsManager.GRID_SIZE)) {
			Static.dialogWithText(this, getString(R.string.wrong_grid_size));
		}

		// train neural network only if is in LocalizeActivity and Neural is enabled
		if (childActivity instanceof LocalizeActivity && SettingsManager.isScanAlgorythmNeuralNetwork()) {
			//scanResultsFloors.getNeuralNetworkHolder().train();
			new AsyncTrainNeuralNetwork(scanResultsFloors,scanResultsFloors.getNeuralNetworkHolder(), this).execute();
		}
		
		loadMap();
		
		// setup Drawer
		mTitle = mDrawerTitle = getTitle();
		mFloorsTitles = Static.getFloorsArray(this);
		
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);


		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
		// set up the drawer's list view with items and click listener
		mDrawerList.setAdapter(new ArrayAdapter<String>(this, R.layout.drawer_list_item, mFloorsTitles));
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		// enable ActionBar app icon to behave as action to toggle nav drawer
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setHomeButtonEnabled(true);

		// ActionBarDrawerToggle ties together the the proper interactions
		// between the sliding drawer and the action bar app icon
		mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
		mDrawerLayout, /* DrawerLayout object */
		R.drawable.ic_drawer, /* nav drawer image to replace 'Up' caret */
		R.string.drawer_open, /* "open drawer" description for accessibility */
		R.string.drawer_close /* "close drawer" description for accessibility */
		)
		{
			public void onDrawerClosed(View view)
			{
				getSupportActionBar().setTitle(mTitle);
			}

			public void onDrawerOpened(View drawerView)
			{
				getSupportActionBar().setTitle(mDrawerTitle);
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		//selectItem(0);
		// DRAWER CONFIG END
		
		
	}
	
	
	// on pause let's clear the tiles bitmap data
	@Override
	public void onPause(){
		super.onPause();
		MapHelper.mapView.clear();
		wl.release();
	}
	
	// on resume, get a new render
	@Override
	public void onResume(){
		super.onResume();
		MapHelper.mapView.requestRender();
		wl.acquire();
	}

	// on destroy, clear and recycle bitmap data, and set all references to null
	@Override
	public void onDestroy(){
		super.onDestroy();
		MapHelper.mapView.destroy();
		MapHelper.mapView = null;
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// The action bar home/up action should open or close the drawer.
		// ActionBarDrawerToggle will take care of this.
		if (mDrawerToggle.onOptionsItemSelected(item))
		{
			return true;
		}
		
		if (item.getItemId() == R.id.settings) {
			Intent intent = new Intent(childActivity, SettingsActivity.class);
	        startActivity(intent);
		}

		return super.onOptionsItemSelected(item);
	}

	/* The click listner for ListView in the navigation drawer */
	private class DrawerItemClickListener implements ListView.OnItemClickListener
	{
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id)
		{
			selectItem(position);
		}
	}

	public void selectItem(int position)
	{
		// update selected item and title, then close the drawer
		mDrawerList.setItemChecked(position, true);
		setTitle(mFloorsTitles[position]);
		MapHelper.currentFloor = position;
		loadMap();
		((MapUpdated)childActivity).mapUpdated();
		
		mDrawerLayout.closeDrawer(mDrawerList);
	}

	@Override
	public void setTitle(CharSequence title)
	{
		mTitle = title;
		getSupportActionBar().setTitle(mTitle);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState)
	{
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		if (mDrawerToggle != null) {
			mDrawerToggle.syncState();
		}
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}
	
	public void loadMap() {
		FrameLayout frame = (FrameLayout) findViewById(R.id.content_frame);
		frame.removeView(MapHelper.mapView);
		
		MapView mapView = new MapView(this);
		MapHelper.setMapView(mapView);
		
		// add mapView to ContentView
		//addContentView(mapView, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.MATCH_PARENT));
		frame.addView(mapView,new DrawerLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.MATCH_PARENT));
		
		String stringLevel = String.valueOf(MapHelper.currentFloor);
		// add images
		
		// OLD maps
		//mapView.addZoomLevel(600, 600, stringLevel+"/600q_%row%_%col%.png", stringLevel+"/q_600.png",300, 300);
		//mapView.addZoomLevel(1200, 1200, stringLevel+"/1200q_%row%_%col%.png",stringLevel+"/q_1200.png",600,600);
		
		// THIS IS ORIGINAL, IT IS WORKING!!
		mapView.addZoomLevel(Constants.MAP_MAX_RESOLUTION_X,Constants.MAP_MAX_RESOLUTION_Y , stringLevel+"/%row%_%col%.png",stringLevel+"/0_0.png",Constants.MAP_MAX_RESOLUTION_X,Constants.MAP_MAX_RESOLUTION_Y);
		
		//mapView.addZoomLevel(2400, 2400, stringLevel+"/2400px_%row%_%col%.png", stringLevel+"/1200px.png",1200, 1200);
		//mapView.addZoomLevel(3600, 3600, stringLevel+"/3600px_%row%_%col%.png", stringLevel+"/1200px.png",1200, 1200);
		
		
		// disable touches
		mapView.setShouldIntercept( false ); // default is true
		
		// disable cache
		mapView.setCacheEnabled( false ); // default is false
		
		// provide the corner coordinates for relative positioning
		mapView.registerGeolocator( Constants.GPS_COORDINATE_LEFT, Constants.GPS_COORDINATE_TOP, Constants.GPS_COORDINATE_RIGHT, Constants.GPS_COORDINATE_BOTTOM);
		
		// set the start scale 
		mapView.setScale( Constants.START_SCALE );
		
		
	}
	
	
	public static void setChildActivity(ActionBarActivity activiy) throws Exception{
		if (activiy instanceof MapUpdated) {
			DefaultMapActivity.childActivity = activiy;
			if (activiy instanceof LocalizeActivity) {
				childActivityType = Constants.LOCALIZE_ACTIVITY;
			}
			else if (activiy instanceof ScanActivity) {
				childActivityType = Constants.SCANNER_ACTIVITY;
			}
		}
		else {
			Static.log("Nelze priradit childActivity, objekt neimplementuje rozhrani MapUpdated");
			throw new Exception(Constants.EXC_classDontImplementMapUpdatedInterface);
		}
	}
	
	public static Activity getChildActivity(){
		return childActivity;
	}
	
	public static int getChildActivityType(){
		return childActivityType;
	}
	
	

}
