package cz.mendelu.wifilocalizator;

import java.io.File;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.qozix.mapview.MapView;

import cz.mendelu.wifilocalizator.async.AsyncProcessScannedData;
import cz.mendelu.wifilocalizator.dataholers.DataLayerType;
import cz.mendelu.wifilocalizator.dialogs.DialogSelectBssid;
import cz.mendelu.wifilocalizator.interfaces.MapUpdated;
import cz.mendelu.wifilocalizator.managers.ImageSquareManager;
import cz.mendelu.wifilocalizator.other.Constants;
import cz.mendelu.wifilocalizator.other.MapHelper;
import cz.mendelu.wifilocalizator.other.Static;
import cz.mendelu.wifilocalizator.wifiscan.WifiScanTimer;

public class ScanActivity extends DefaultMapActivity implements View.OnClickListener, MapUpdated{

	ImageSquareManager squareManager;
    WifiScanTimer wifiScanTimer;
    Menu menu;
    
    // Views
	Button buttonStartScan;
    RelativeLayout scanBar;
    TextView textScanning;
    ProgressBar progressScan;
	
	@Override
	public void onCreate( Bundle savedInstanceState ) {
		try {
			DefaultMapActivity.setChildActivity(this);
			super.onCreate( savedInstanceState );
			squareManager = new ImageSquareManager(scanResultsFloors.getCurrentScanResultsGrid() ,this);
					
			LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			// add mapOverlay to ContentView
			View mapOverlay = inflater.inflate(R.layout.scan_activity_overlay, null);
			buttonStartScan = (Button) mapOverlay.findViewById(R.id.buttonStartScan);
			buttonStartScan.setOnClickListener(this);
			scanBar = (RelativeLayout) mapOverlay.findViewById(R.id.scannerbar);
			textScanning = (TextView) mapOverlay.findViewById(R.id.textScanning);
			progressScan = (ProgressBar) mapOverlay.findViewById(R.id.progressBarScanning);
			
			addContentView(mapOverlay, new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,RelativeLayout.LayoutParams.MATCH_PARENT));
			
			setUpMapEventListener();
		}
		catch (Exception e){
			Static.logExceptionDetail(e);
		}
		
	}
	
	
	

	@Override
	public void onClick(View v) {
		try {
			if (v.getId() == R.id.buttonStartScan) {
				if ( WifiScanTimer.isScanning() ) {
					// set-up views
					buttonStartScan.setText(getString(R.string.start));
					scanBar.setVisibility(View.INVISIBLE);
					textScanning.setVisibility(View.INVISIBLE);
					progressScan.setVisibility(View.INVISIBLE);
					buttonStartScan.setVisibility(View.INVISIBLE);
					
					// useful code
					squareManager.enableAddingSquaresForScan();
					wifiScanTimer.cancel();
					new AsyncProcessScannedData(scanResultsFloors.getCurrentScanResultsGrid(), squareManager, wifiScanTimer, this).execute();
				}
				else {
					// set-up view
					scanBar.setVisibility(View.VISIBLE);
					textScanning.setVisibility(View.VISIBLE);
					progressScan.setVisibility(View.VISIBLE);
					buttonStartScan.setVisibility(View.VISIBLE);					
					
					// useful code
					wifiScanTimer = new WifiScanTimer(getApplicationContext(),squareManager.getNumberOfNewScanSquares());
					wifiScanTimer.start();
					buttonStartScan.setText(getString(R.string.stop));
					squareManager.disaleAddingSquaresForScan();
				}
			}
		}
		catch (Exception e){
			Static.showToast(this, ""+e.getMessage());
			Static.logExceptionDetail(e);
		}
			
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    this.menu = menu;
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.scan, menu);
	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.show_data_layer) {
			if (! scanResultsFloors.getCurrentScanResultsGrid().isEmpty()) {
				new DialogSelectBssid(this, scanResultsFloors.getCurrentScanResultsGrid(),squareManager,menu);
			}
			else {
				Static.showToast(this, getString(R.string.not_data_for_current_floor));
				squareManager.getDataLayerType().setDataLayerType(Constants.DATA_LAYER_NONE);
				squareManager.reloadDataLayer();
				menu.findItem(R.id.show_data_layer).setIcon(R.drawable.ic_action_make_available_offline);
			}
		}
		if (item.getItemId() == R.id.delete_xml_data) {
			File sdCard = Environment.getExternalStorageDirectory();
			File dir = new File (sdCard.getAbsolutePath() + Constants.XML_DATA_DIR);
			File file = new File(dir, Constants.XML_DATA_FILENAME+MapHelper.currentFloor+Constants.XML_DATA_SUFFIX);
			file.delete();
			Static.goToScanActivity(this);
		}
		if (item.getItemId() == R.id.switch_to_localize) {
			Static.goToLocalizeActivity(this);
		}
		if (item.getItemId() == R.id.delete_sharedpref_data) {
			settingsManager.deleteAll();
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onStop() {
		squareManager.cleanUpImage();
		squareManager.cleanUpImageForScan();
		super.onStop();
	}

	private void setUpMapEventListener() {
		MapHelper.mapView.addMapEventListener(new MapView.MapEventListener() {
			
			@Override
			public void onZoomStart(double scale) {
				
			}
			
			@Override
			public void onZoomLevelChanged(int oldZoom, int currentZoom) {
				
			}
			
			@Override
			public void onZoomComplete(double scale) {
				
			}
			
			@Override
			public void onTap(int x, int y) {
				try {
					if (squareManager.isCurrentLayerScanLayer()) {
						int[] absolutePosition = MapHelper.relativePositionToAbsolute(x, y);
						squareManager.addSquareForScan(absolutePosition);
						hideOrShowScanButton();
					}
				}
				catch (Exception e){
					Static.logExceptionDetail(e);
				}
			}
			
			@Override
			public void onScrollChanged(int x, int y) {
				
			}
			
			@Override
			public void onScaleChanged(double scale) {
				squareManager.onScaleChanged(scale);
			}
			
			@Override
			public void onRenderStart() {
				
			}
			
			@Override
			public void onRenderComplete() {
				
			}
			
			@Override
			public void onPinchStart(int x, int y) {
				
			}
			
			@Override
			public void onPinchComplete(int x, int y) {

			}
			
			@Override
			public void onPinch(int x, int y) {
				
			}
			
			@Override
			public void onFlingComplete(int x, int y) {
				
			}
			
			@Override
			public void onFling(int sx, int sy, int dx, int dy) {
				
			}
			
			@Override
			public void onFingerUp(int x, int y) {

				
			}
			
			@Override
			public void onFingerDown(int x, int y) {
				
			}
			
			@Override
			public void onDrag(int x, int y) {

			}
			
			@Override
			public void onDoubleTap(int x, int y) {
				
			}
		});
	}

	@Override
	public void onResume() {
		Static.log("ScanAcitivity: onResume");
		super.onResume();
	}
	
	@Override
	public void mapUpdated() {
		setUpMapEventListener();
		squareManager.cleanUpImage();
		squareManager.cleanUpImageForScan();
		squareManager = new ImageSquareManager(scanResultsFloors.getCurrentScanResultsGrid() ,squareManager.getDataLayerType(),this);
		squareManager.reloadDataLayer();
		hideOrShowScanButton();
	}
	
	public void hideOrShowScanButton(){
		if (squareManager.getNumberOfNewScanSquares() > 0) {
			buttonStartScan.setVisibility(View.VISIBLE);
			scanBar.setVisibility(View.VISIBLE);
		}
		else {
			buttonStartScan.setVisibility(View.INVISIBLE);
			scanBar.setVisibility(View.INVISIBLE);
		}
	}


}
