package cz.mendelu.wifilocalizator;

import cz.mendelu.wifilocalizator.other.Constants;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;

public class SettingsActivity extends ActionBarActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		setContentView(R.layout.settings_activity);
		
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.replace(R.id.settings_frame, new SettingsFragment());
		ft.commit();
		
	}
	
	
	@Override
	public void onBackPressed() {
		goToPreviousActivity();
		super.onBackPressed();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	    case android.R.id.home:
	    	goToPreviousActivity();
	    }
	    return super.onOptionsItemSelected(item);
	}
	
	private void goToPreviousActivity(){
		if (DefaultMapActivity.getChildActivityType() == Constants.LOCALIZE_ACTIVITY) {
    		Intent intent = new Intent(this, LocalizeActivity.class);
    		startActivity(intent);
    	}
    	else if (DefaultMapActivity.getChildActivityType() == Constants.SCANNER_ACTIVITY){
    		Intent intent = new Intent(this, ScanActivity.class);
    		startActivity(intent);
    	}
	}
	
}
