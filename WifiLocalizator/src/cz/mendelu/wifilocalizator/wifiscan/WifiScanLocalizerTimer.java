package cz.mendelu.wifilocalizator.wifiscan;

import java.util.Timer;

import cz.mendelu.wifilocalizator.LocalizeActivity;
import cz.mendelu.wifilocalizator.LocalizeActivity.LocalizeActivityHandler;
import cz.mendelu.wifilocalizator.dataholers.ScanResultsFloors;
import cz.mendelu.wifilocalizator.other.Static;

import android.app.Activity;
import android.content.Context;

public class WifiScanLocalizerTimer extends Timer{
	WifiScanLocalizerTimerTask wifiScanLocalizerTimerTask;
	
	public WifiScanLocalizerTimer(LocalizeActivity activity,ScanResultsFloors scanResultsFloors,LocalizeActivityHandler handler) {
		try {
			wifiScanLocalizerTimerTask = new WifiScanLocalizerTimerTask(activity, scanResultsFloors, handler);
		}
		catch (Exception e){
			Static.logExceptionDetail(e);
		}
	}
	
	public void start(){
		WifiScanLocalizerTimerTask.enableRunNext();
		super.schedule(wifiScanLocalizerTimerTask, 0);
	}
	
	public static void stop(){
		WifiScanLocalizerTimerTask.disableRunNext();
	}

}
