package cz.mendelu.wifilocalizator.wifiscan;

import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources.Theme;
import android.net.wifi.WifiManager;
import android.os.Message;
import cz.mendelu.wifilocalizator.LocalizeActivity;
import cz.mendelu.wifilocalizator.LocalizeActivity.LocalizeActivityHandler;
import cz.mendelu.wifilocalizator.dataholers.FindResutlHolder;
import cz.mendelu.wifilocalizator.dataholers.ScanResult;
import cz.mendelu.wifilocalizator.dataholers.ScanResultAp;
import cz.mendelu.wifilocalizator.dataholers.ScanResultsFloors;
import cz.mendelu.wifilocalizator.managers.SettingsManager;
import cz.mendelu.wifilocalizator.other.Constants;
import cz.mendelu.wifilocalizator.other.Static;



public class WifiScanLocalizerTimerTask extends TimerTask {
    WifiManager wifi;       
    LocalizeActivity activity;
    List<ScanResult> scanResultsHolder = new ArrayList<ScanResult>();
    ScanResultsFloors scanResultsFloors;
    LocalizeActivityHandler handler;
    static Boolean runNext = true;
	
	public WifiScanLocalizerTimerTask(LocalizeActivity activity,ScanResultsFloors scanResultsFloors,LocalizeActivityHandler handler) throws Exception{
		this.wifi = (WifiManager) activity.getSystemService(Context.WIFI_SERVICE);
		this.scanResultsFloors = scanResultsFloors;
		this.handler = handler;
		this.activity = activity;
	}

	@Override
	public void run() {
		try {
			long startTime = (System.currentTimeMillis());
			scanWifi();
			FindResutlHolder foundPositions = scanResultsFloors.findPosition(getScanResult(),activity.testHolder); 
			sendMessage(foundPositions);
			Static.log("WifiScanLocalizer dokoncen za: "+(System.currentTimeMillis() - startTime));
			
			if (runNext) {
				new WifiScanLocalizerTimer(activity, scanResultsFloors, handler).start();
			}
			else {
				Static.log("Nebudu jiz poustet WifiScanLocalizer");
			}
		}
		catch (Exception e){
			Static.logExceptionDetail(e);
		}
	}
	
	private void scanWifi() throws Exception{
	//	long startTime = (System.currentTimeMillis());
	//	Static.log("Startuji scan...");
		for (int i=0; i < SettingsManager.FIND_POSITION_SCAN_COUNT; i++) {
			Boolean scanSucces = wifi.startScan();
			// scan OK
			if (scanSucces) {
				List<android.net.wifi.ScanResult> scanResults = wifi.getScanResults();
				ScanResult dataScanResult = new ScanResult();
					if (scanResults != null) {
			        	for (android.net.wifi.ScanResult result : scanResults) {
			                dataScanResult.addForceNoSort(new ScanResultAp(result.SSID,result.BSSID, (result.level+SettingsManager.GAIN_CORRECTION)));
			        	}
					}
		        scanResultsHolder.add(dataScanResult);
			}
			// scan fail
			else {
				Static.log("Nepodarilo se scanovat wifi site");
				throw new Exception(Constants.EXC_notAbleToScanWifiNetwork);
			}
			
			// sleep 
	        try {
				Thread.sleep( (SettingsManager.FIND_POSITION_SCAN_INTERVAL) );
			} catch (Exception e) {
				Static.logExceptionDetail(e);
			}
		}
	//	Static.log("Scan ukoncen za: "+(System.currentTimeMillis()-startTime));
	}
	
	
	private void sendMessage(FindResutlHolder findResults){
		if (findResults != null) {
			Message msg = new Message();
	    	msg.setTarget(handler);
	    	msg.setData(findResults.getBundle());
	    	msg.sendToTarget();
		}
	}
	
		
	private ScanResult getScanResult() throws Exception{
		ScanResult scanResult = new ScanResult();
		for (ScanResult r : scanResultsHolder) {
			scanResult.addWithAverage(r);
		}
		return scanResult;
	}
	
	public static void disableRunNext(){
		runNext = false;
	}
	
	public static void enableRunNext(){
		runNext = true;
	}
	
	
}