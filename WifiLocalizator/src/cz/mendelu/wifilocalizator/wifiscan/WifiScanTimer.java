package cz.mendelu.wifilocalizator.wifiscan;

import java.util.List;
import java.util.Timer;

import android.content.Context;
import cz.mendelu.wifilocalizator.dataholers.ScanResult;
import cz.mendelu.wifilocalizator.dataholers.ScanResultAp;
import cz.mendelu.wifilocalizator.managers.SettingsManager;
import cz.mendelu.wifilocalizator.other.Static;

public class WifiScanTimer extends Timer{
	WifiScanTimerTask wifiScanTimerTask;
	static Boolean isScanning = false;
	
	public WifiScanTimer(Context context,int numberOfSquares) throws Exception{
		this.wifiScanTimerTask = new WifiScanTimerTask(context,numberOfSquares);
	}
	
	public WifiScanTimer(WifiScanTimerTask wifiScanTimerTask) {
		this.wifiScanTimerTask = wifiScanTimerTask;
	}
	
	@Override
	public void cancel() {
		isScanning = false;
		wifiScanTimerTask.saveStopTime();
		super.cancel();
	}
	
	
	public void start() {
		isScanning = true;
		super.scheduleAtFixedRate(wifiScanTimerTask, 0, (long)(SettingsManager.SCANNER_SCAN_INTERVAL*1.1));
	}
	
	public static Boolean isScanning(){
		return isScanning;
	}
	
	public void printScannedSquares() throws Exception{
		int i = 1;
		for (ScanResult r : wifiScanTimerTask.getScannedSquares()) {
			Static.log("=============");
			Static.log("Square num: "+i);
			for (ScanResultAp ap: r.getListResult()) {
				Static.log("SSID: "+ap.getSsid()+" BSSID: "+ap.getBssid()+" Level: "+ap.getLevel());
			}
			i++;
		}
	}
	
	public List<ScanResult> getScannedSquares() throws Exception {
		return wifiScanTimerTask.getScannedSquares();
	}

}
