package cz.mendelu.wifilocalizator.wifiscan;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TimerTask;

import android.content.Context;
import android.net.wifi.WifiManager;
import cz.mendelu.wifilocalizator.dataholers.ScanResult;
import cz.mendelu.wifilocalizator.dataholers.ScanResultAp;
import cz.mendelu.wifilocalizator.other.Constants;
import cz.mendelu.wifilocalizator.other.Static;



public class WifiScanTimerTask extends TimerTask {
    WifiManager wifi;       
    long startTime = 0;
    long stopTime = 0;
    int numberOfSquares = 0;
    List<ScanResult> scanResultsHolder = new ArrayList<ScanResult>(); 
	
	public WifiScanTimerTask(Context context,int numberOfSquares) throws Exception{
		this.wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		this.numberOfSquares = numberOfSquares;
		if (numberOfSquares == 0) {
			throw new Exception(Constants.EXC_numberOfSquaresIsZero);
		}
	}

	@Override
	public void run() {
		double start = System.currentTimeMillis();
		//Static.log("Scan start!!");
		if (startTime == 0) {
			Static.log("Ukladam startTime");
			startTime = System.currentTimeMillis();
		}
		// scan wifi
		if (Constants.DEBUG_MSG_WIFI_SCAN) Static.log("================");
		wifi.startScan();
		List<android.net.wifi.ScanResult> scanResults = wifi.getScanResults();
		ScanResult dataScanResult = new ScanResult();
        	for (android.net.wifi.ScanResult result : scanResults) {
        		if (Constants.DEBUG_MSG_WIFI_SCAN) {
	        		Static.log("------");
	                Static.log("SSID: "+result.SSID);
	                Static.log("BSID: "+result.BSSID);
	                Static.log("Level: "+result.level);
        		}
                dataScanResult.addForceNoSort(new ScanResultAp(result.SSID,result.BSSID, result.level));
        	}
        scanResultsHolder.add(dataScanResult);	
        //Static.log("Scan dokoncen za:"+(System.currentTimeMillis()-start));
	}
	
	public void saveStopTime(){
		Static.log("Ukladam stopTime");
		stopTime = System.currentTimeMillis();
	}
	
		
	public List<ScanResult> getScannedSquares() throws Exception{
		List<ScanResult> scannedSquares = new ArrayList<ScanResult>();
		
		// reverse order
		List<ScanResult> reversedScanResult = new ArrayList<ScanResult>(scanResultsHolder);
		Collections.reverse(reversedScanResult);
		
		Static.log("Pocet prevedenych mereni: "+reversedScanResult.size());
		
		// calculate number of samples in one square
		int numberOfSamplesInSquare = reversedScanResult.size() / numberOfSquares;
		
		Static.log("Pocet mereni na ctverec: "+numberOfSamplesInSquare);
		
		int i = 1;
		ScanResult newResult = new ScanResult();
		for(ScanResult r : reversedScanResult) {
			newResult.addWithAverage(r);
			if (i == numberOfSamplesInSquare) {
				i = 1;
				Static.log("Ukladam ctverec: "+i);
				scannedSquares.add(newResult);
				newResult = new ScanResult();
			}
			i++;
		}
		
		Static.log("Pocet ctveru k predani: "+scannedSquares.size());
		Static.log("Pocet AP v prvnim ctverci:"+scannedSquares.get(0).getScanResultsCount());
		
		return scannedSquares;
	}
	
	
}