package cz.mendelu.wifilocalizator.dialogs;

import java.util.ArrayList;
import java.util.List;

import cz.mendelu.wifilocalizator.R;
import cz.mendelu.wifilocalizator.R.string;
import cz.mendelu.wifilocalizator.dataholers.ScanResultsGrid;
import cz.mendelu.wifilocalizator.managers.ImageSquareManager;
import cz.mendelu.wifilocalizator.other.Constants;
import cz.mendelu.wifilocalizator.other.Static;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.internal.view.menu.ActionMenuItemView;
import android.view.Menu;
import android.view.MenuItem;

public class DialogSelectBssid {
	
	public DialogSelectBssid(final Context context,ScanResultsGrid scanResultsGrid,final ImageSquareManager squareManager,final Menu menu) {
		 // create list
		 //final List<String> bssidStringList = scanResultsGrid.getAllBssid();
		 final List<String> bssidStringList = scanResultsGrid.getAllBssidSortedByCount();
		 final List<String> bssidStringListWithNavigation = new ArrayList<String>();
		 bssidStringListWithNavigation.add(Constants.DATA_LAYER_NONE);
		 bssidStringListWithNavigation.add(Constants.DATA_LAYER_ALL);
		 bssidStringListWithNavigation.addAll(bssidStringList);
		 CharSequence[] bssidList = bssidStringListWithNavigation.toArray(new CharSequence[bssidStringList.size()]);
		 // last known datatype
		 int pos = 0;
		 for (String str : bssidStringListWithNavigation) {
			 if (str.matches(squareManager.getDataLayerType().getDataLayerType())) {
				break; 
			 }
			 pos++;
		 }
		 
		 AlertDialog.Builder builder = new AlertDialog.Builder(context);
         builder.setTitle(context.getText(R.string.choose_bssid));
         builder.setSingleChoiceItems(bssidList, pos, new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface arg0, int position) {
				changeActionBarIcon(menu, position);
				squareManager.getDataLayerType().setDataLayerType(bssidStringListWithNavigation.get(position));
				squareManager.reloadDataLayer();
				arg0.dismiss();
			}
		});
        AlertDialog dialogSelectBssid = builder.create();
        dialogSelectBssid.show();
	}
	
	private void changeActionBarIcon(Menu menu,int position){
		MenuItem mi = menu.findItem(R.id.show_data_layer);
		if (position == 0) {
			mi.setIcon(R.drawable.ic_action_make_available_offline);
		}
		else {
			mi.setIcon(R.drawable.ic_action_av_add_to_queue);
		}
	}
	
	
}
