package cz.mendelu.wifilocalizator.managers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Message;
import android.view.Gravity;
import android.widget.TextView;
import cz.mendelu.wifilocalizator.R;
import cz.mendelu.wifilocalizator.dataholers.FindResult;
import cz.mendelu.wifilocalizator.dataholers.FindResutlHolder;
import cz.mendelu.wifilocalizator.other.BitmapCreator;
import cz.mendelu.wifilocalizator.other.Constants;
import cz.mendelu.wifilocalizator.other.MapHelper;
import cz.mendelu.wifilocalizator.other.Static;

public class ImageLocationManager {
	Context context;
	List<TextView> markers = new ArrayList<TextView>(); 
	FindResutlHolder findResutlHolder;
	public static Boolean FIND_POSITION_SHOW_ALL_FOUND_SQUARES = false;

	
	public ImageLocationManager(Context context) {
		this.context = context;
	}
	
	
	public Boolean isMarkerAtPosition(int x,int y){
		for(TextView tx : markers) {
			int[] txPosition = (int[]) tx.getTag();
			if (txPosition[0] == x && txPosition[1] == y) {
				return true;
			}
		}
		return false;
	}
	
	public FindResutlHolder getFindResultHolder(){
		return findResutlHolder;
	}
	
	public void handleMsg(Message msg) {
		//long startTime = (System.currentTimeMillis());
		//Static.log("Zobrazuji data...");
		cleanUp();
		findResutlHolder = new FindResutlHolder(msg.getData());
		
		// show results only if is showing right floor
		if (findResutlHolder.getFloor() == MapHelper.currentFloor) {
			if ( FIND_POSITION_SHOW_ALL_FOUND_SQUARES) {
				addAllResults(findResutlHolder.getAll());
			}
			else {
				addBestResults(findResutlHolder.getBestFitness());
			}
		}
		else {
			Static.log("Neukazu zadne vysledky protoze zobrazena mapa je jineho z parta nez vysledky");
		}
		//Static.log("Data zobrazena za: "+(System.currentTimeMillis()-startTime));
	}
	
	private void addBestResults(List<FindResult> results){
			int bestfitness = getBestFitness(results);
			for (int i=0;i<results.size();i++) {
				FindResult fr = results.get(i);
				if (fr.getFitness() == bestfitness) {
					addPosition(fr.getX(), fr.getY(),fr.getFitness(),true);
				}
				else {
					addPosition(fr.getX(), fr.getY(),fr.getFitness(),false);
				}
			}
	}
	
	private void addAllResults(List<FindResult> results) {
		for (FindResult fr : results) {
			addPosition(fr.getX(), fr.getY(),fr.getFitness(),false);
		}
	}
	
	private int getBestFitness(List<FindResult> results){
		int bestFitness = Integer.MAX_VALUE;
		for (FindResult fr : results) {
			if (fr.getFitness() < bestFitness) {
				bestFitness = fr.getFitness();
			}
		}
		return bestFitness;
	}
	
	
	private void addPosition(int x,int y,int fitness,Boolean redColor) {
		if (x != Integer.MAX_VALUE && y != Integer.MAX_VALUE) {
			int squareSizeMultipliter = 0;
			TextView position = new TextView(context);
			if (redColor) {
				// user size square size
				squareSizeMultipliter = 1;
				position.setBackgroundDrawable(BitmapCreator.getUserSizeSquareBitmapDrawable(BitmapCreator.COLOR_RED));
				//position.setBackgroundDrawable(BitmapCreator.getSquareBitmapDrawable(BitmapCreator.COLOR_RED));
			}
			else {
				// TESTING ENABLING SIZE MULTIPLIER FOR BLUE SQUARES
				squareSizeMultipliter = 1;
				position.setBackgroundDrawable(BitmapCreator.getSquareBitmapDrawable(BitmapCreator.COLOR_BLUE));
			}
			position.setTag( new int[] { x,y,squareSizeMultipliter } );
			position.setText(fitness+"");
			position.setTextSize(15);
			position.setTextColor(R.color.black);
			position.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
			int[] trimmPosition = MapHelper.gridPositionToTrimPosition(new int[] {x,y});
			MapHelper.mapView.addMarker(position, trimmPosition[0], trimmPosition[1],true);
			markers.add(position);
			onScaleChanged(MapHelper.mapView.getScale());
		}
	}
	
	public void cleanUp(){
		for (TextView t : markers) {
			MapHelper.mapView.removeMarker(t);
		}
		markers.clear();
	}
	
	public void onScaleChanged(double scale) {
		for (TextView textview : markers) {
			int sizeMultipliter = ((int[])(textview.getTag()))[2];
			
			if (sizeMultipliter == 0) {
				textview.getLayoutParams().height = (int) ( Constants.CELL_GRID_SIZE_X*scale);
				textview.getLayoutParams().width = (int) ( Constants.CELL_GRID_SIZE_Y*scale);
			}
			else {
				textview.getLayoutParams().height = (int) ( SettingsManager.FIND_POSITION_SIZE_OF_RESULT_MULTIPLE*Constants.CELL_GRID_SIZE_X*scale);
				textview.getLayoutParams().width = (int) ( SettingsManager.FIND_POSITION_SIZE_OF_RESULT_MULTIPLE*Constants.CELL_GRID_SIZE_Y*scale);
			}
		}
	}

}
