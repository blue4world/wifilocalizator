package cz.mendelu.wifilocalizator.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import cz.mendelu.wifilocalizator.R;
import cz.mendelu.wifilocalizator.other.Constants;
import cz.mendelu.wifilocalizator.other.Static;

public class SettingsManager {
	// constans
	static final int SCAN_ALGORYTHM_AP_ORDER = 0;
	static final int SCAN_ALGORYTHM_SIGNAL_DIFF = 1;
	static final int SCAN_ALGORYTHM_NEURAL_NETWORK = 2;
	static final int SCAN_ALGORYTHM_EUCLIDEAN_DISTANCE = 3;
	static final int SCAN_ALGORYTHM_EUCLIDEAN_DISTANCE_WITH_SHIFT = 4;
	
	
	static SharedPreferences sharedPrefs;
	static Context context;
	public static int FIND_POSITION_NUMBER_OF_ACCEPTED_AP;
	public static int FIND_POSITION_ALGORYTHM;
	public static int FIND_POSITION_SCAN_INTERVAL;
	public static int FIND_POSITION_SCAN_COUNT;
	public static int SCANNER_SCAN_INTERVAL;
	public static int FIND_POSITION_NUM_OF_RESULT_TO_SHOW;
	public static int FIND_POSITION_SIZE_OF_RESULT_MULTIPLE;
	public static int GAIN_CORRECTION;
	public static int EDUROAM_ONLY;
	public static int GRID_SIZE;
	public static int SHIFT_SIZE;
	
	
	public static void init(Context context) {
		sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
		SettingsManager.context = context;
		
		loadSettings();
		reloadConstans();
		printCurrentSettings();
	}
	
	
	private static void loadSettings(){
		try {
			// FIND_POSITION_NUMBER_OF_ACCEPTED_AP
			FIND_POSITION_NUMBER_OF_ACCEPTED_AP = Integer.valueOf(sharedPrefs.getString(context.getString(R.string.sharedpref_num_of_ap_to_use), context.getString(R.string.sharedpref_default_num_of_ap_to_use)));
			
			// FIND_POSITION_ALGORYTHM
			FIND_POSITION_ALGORYTHM = Integer.parseInt(sharedPrefs.getString(context.getString(R.string.sharedpref_find_algorhythm), context.getString(R.string.sharedpref_default_find_algorhythm))); 
						
			// FIND_POSITION_SCAN_INTERVAL
			FIND_POSITION_SCAN_INTERVAL = Integer.parseInt(sharedPrefs.getString(context.getString(R.string.sharedpref_find_position_scan_interval), context.getString(R.string.sharedpref_default_find_position_scan_interval)));
			
			// FIND_POSITION_SCAN_COUNT
			FIND_POSITION_SCAN_COUNT = Integer.parseInt(sharedPrefs.getString(context.getString(R.string.sharedpref_find_position_scan_count), context.getString(R.string.sharedpref_default_find_position_scan_count)));
			
			// SCANNER_SCAN_INTERVAL  
			SCANNER_SCAN_INTERVAL = Integer.parseInt(sharedPrefs.getString(context.getString(R.string.sharedpref_scanner_scan_interval), context.getString(R.string.sharedpref_default_scanner_scan_interval)));
			
			// FIND_POSITION_NUM_OF_RESULT_TO_SHOW
			FIND_POSITION_NUM_OF_RESULT_TO_SHOW = Integer.valueOf(sharedPrefs.getString(context.getString(R.string.sharedpref_find_position_num_of_result_to_show), context.getString(R.string.sharedpref_default_find_position_num_of_result_to_show)));
			
			// FIND_POSITION_SIZE_OF_RESULT_MULTIPLE
			FIND_POSITION_SIZE_OF_RESULT_MULTIPLE = Integer.valueOf(sharedPrefs.getString(context.getString(R.string.sharedpref_find_position_size_of_result_multiple_constant), context.getString(R.string.sharedpref_detault_find_position_size_of_result_multiple_constant)));
			
			// GAIN CORRECTION
			GAIN_CORRECTION = Integer.valueOf(sharedPrefs.getString(context.getString(R.string.sharedpref_general_gain_correction), context.getString(R.string.sharedpref_default_general_gain_correction)));
			
			// EDUROAM ONLY
			EDUROAM_ONLY = Integer.valueOf(sharedPrefs.getString(context.getString(R.string.sharedpref_general_eduroam_only), context.getString(R.string.sharedpref_default_general_eduroam_only)));
			
			// GRID_SIZE
			GRID_SIZE = Integer.valueOf(sharedPrefs.getString(context.getString(R.string.sharedpref_general_grid_size), context.getString(R.string.sharedpref_defaul_general_grid_size)));
			
			// SHIFT_SIZE
			SHIFT_SIZE =  Integer.valueOf(sharedPrefs.getString(context.getString(R.string.sharedpref_find_position_shift_size), context.getString(R.string.sharedpref_default_find_position_shift_size)));
		}
		catch (Exception e){
			Static.log("Neporadilo se korektne nacist nastaveni");
			Static.logExceptionDetail(e);
		}
	}
	
	
	public static Boolean isScanAlgorythmSignalDiff(){
		if (FIND_POSITION_ALGORYTHM == SCAN_ALGORYTHM_SIGNAL_DIFF) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public static Boolean isScanAlgorythmApOrder(){
		if (FIND_POSITION_ALGORYTHM == SCAN_ALGORYTHM_AP_ORDER) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public static Boolean isScanAlgorythmNeuralNetwork(){
		if (FIND_POSITION_ALGORYTHM == SCAN_ALGORYTHM_NEURAL_NETWORK) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public static Boolean isScanAlgorythmEuclideanDistance(){
		if (FIND_POSITION_ALGORYTHM == SCAN_ALGORYTHM_EUCLIDEAN_DISTANCE) {
			return true;
		}
		else {
			return false;
		}
		
	}
	
	public static Boolean isScanAlgorythmEuclideanDistanceWithShift(){
		if (FIND_POSITION_ALGORYTHM == SCAN_ALGORYTHM_EUCLIDEAN_DISTANCE_WITH_SHIFT) {
			return true;
		}
		else {
			return false;
		}
		
	}
	
	public void deleteAll(){
		Static.log("Mazu sharedPref data...");
		sharedPrefs.edit().clear().commit();
		loadSettings();
	}

	
	private static void printCurrentSettings(){
		Static.log("Pocet braných AP: "+FIND_POSITION_NUMBER_OF_ACCEPTED_AP);
		Static.log("Vyhledavaci algoritmus na: "+FIND_POSITION_ALGORYTHM+" O=order, 1=diff 2=neural 3=eucliedan distance 4=eucliedan distance with shift");
		Static.log("Find position scan interval: "+FIND_POSITION_SCAN_INTERVAL);
		Static.log("Find position pocet scanu: "+FIND_POSITION_SCAN_COUNT);
		Static.log("Scanner scan interval: "+SCANNER_SCAN_INTERVAL);
		Static.log("Find position pocet vysledku k zobrazeni "+FIND_POSITION_NUM_OF_RESULT_TO_SHOW);
		Static.log("Show result velikost ctverce s vyslednou polohou"+FIND_POSITION_SIZE_OF_RESULT_MULTIPLE);
		Static.log("Kompenzace zisku antény: "+GAIN_CORRECTION);
		Static.log("Eduroam only: "+EDUROAM_ONLY);
		Static.log("Grid size: "+GRID_SIZE);
		Static.log("shift size: "+SHIFT_SIZE);
		//Static.log("Integer max value: "+Integer.MAX_VALUE);
	}
	
	
	private static void reloadConstans(){
	    Constants.NUMBER_OF_CELLS_IN_GRID_POWER = (int) Math.pow(SettingsManager.GRID_SIZE, 2);
	    Constants.CELL_GRID_SIZE_X = Constants.MAP_MAX_RESOLUTION_X / SettingsManager.GRID_SIZE;
	    Constants.CELL_GRID_SIZE_Y = Constants.MAP_MAX_RESOLUTION_Y / SettingsManager.GRID_SIZE;
	}
	
	/*
	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,String key) {
		Static.log("onSharedPreferenceChanged bylo zavolano");
		loadSettings();
		printCurrentSettings();
	}
	*/

}
