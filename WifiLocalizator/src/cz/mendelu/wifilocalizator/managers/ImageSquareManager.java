package cz.mendelu.wifilocalizator.managers;

import java.util.ArrayList;

import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import cz.mendelu.wifilocalizator.R;
import cz.mendelu.wifilocalizator.ScanActivity;
import cz.mendelu.wifilocalizator.dataholers.DataLayerType;
import cz.mendelu.wifilocalizator.dataholers.GridSquareOrder;
import cz.mendelu.wifilocalizator.dataholers.ScanResult;
import cz.mendelu.wifilocalizator.dataholers.ScanResultAp;
import cz.mendelu.wifilocalizator.dataholers.ScanResultsGrid;
import cz.mendelu.wifilocalizator.other.BitmapCreator;
import cz.mendelu.wifilocalizator.other.Constants;
import cz.mendelu.wifilocalizator.other.MapHelper;
import cz.mendelu.wifilocalizator.other.Static;

public class ImageSquareManager {	
	ScanActivity activity;
	ArrayList<TextView> squareImagesForScan = new ArrayList<TextView>();
	ArrayList<TextView> squareImages = new ArrayList<TextView>();
	GridSquareOrder gridSquareOrder = new GridSquareOrder();
	ScanResultsGrid scanResultsGrid;
	DataLayerType dataLayerType = new DataLayerType();
	Boolean enableAddingSquaresForScan = true;
	
	public ImageSquareManager(ScanResultsGrid scanResultsGrid,ScanActivity activity) {
		this.activity = activity;
		this.scanResultsGrid = scanResultsGrid;
	}
	
	public ImageSquareManager(ScanResultsGrid scanResultsGrid,DataLayerType dataLayerType,ScanActivity activity) {
		this.activity = activity;
		this.scanResultsGrid = scanResultsGrid;
		this.dataLayerType = dataLayerType;
	}
	
	public void enableAddingSquaresForScan(){
		enableAddingSquaresForScan = true;
	}
	
	public void disaleAddingSquaresForScan(){
		enableAddingSquaresForScan = false;
	}
	
	public void showDataLayer(String bssid){
		cleanUpImage();
		loadSquaresFromScanResultGrid(bssid);
		onScaleChanged(MapHelper.mapView.getScale());
	}
	
	public void reloadDataLayer(){
		if (dataLayerType.getDataLayerType().matches(Constants.DATA_LAYER_ALL)) {
			showDataLayer();
		}
		else if (dataLayerType.getDataLayerType().matches(Constants.DATA_LAYER_NONE)){
			hideDataLayer();
		}
		else {
			showDataLayer(dataLayerType.getDataLayerType().toLowerCase());
		}
	}
	
	
	public void showDataLayer(){
		cleanUpImage();
		loadSquaresFromScanResultGrid();
		onScaleChanged(MapHelper.mapView.getScale());
	}
	
	public void hideDataLayer(){
		cleanUpImage();
	}
	
	
	private void loadSquaresFromScanResultGrid(){

		for(int x = 0; x < SettingsManager.GRID_SIZE; x++) {
			for(int y = 0; y < SettingsManager.GRID_SIZE; y++) {
				if (! scanResultsGrid.isPositionEmpty(x, y) ) {
					Static.log("Nacitam ulozeny ctverec na pozici: "+x+":"+y);
					addSquareImage(x,y,null);
				}
			}
		}
	}
	
	private void loadSquaresFromScanResultGrid(String bssid){
		cleanUpImage();
		for(int x = 0; x < SettingsManager.GRID_SIZE; x++) {
			for(int y = 0; y < SettingsManager.GRID_SIZE; y++) {
				if (! scanResultsGrid.isPositionEmpty(x, y) ) {
					ScanResult scanResult = scanResultsGrid.getScanResultFromCoordinates(new int[] { x, y} );
					Boolean found = false;
					for (ScanResultAp scanResultAp : scanResult.getListResult()) {
						if (scanResultAp.getBssid().matches(bssid)) {
							addSquareImage(x,y,scanResultAp.getLevel()+"");
							found = true;
						}
					}
					if (!found) {
						addSquareImage(x, y, null);
					}
				}
			}
		}
	}
	
	
	
	public void addSquareForScan(int[] absolutePosition) throws Exception{
		if (enableAddingSquaresForScan) {
			int[] trimmedPosition = MapHelper.trimPositionToGrid(absolutePosition);
			if (squareImagesForScan.size() == 0) {
				addSquareImageForScan(trimmedPosition);
			}
			else {
				if (isGoodPositionForAddSquare(trimmedPosition)) {
					addSquareImageForScan(trimmedPosition);
				}
			}
		}
		else {
			Static.log("Pridavani novych ctvercu ke skenovani je zakazano");
		}
	}
	
	public DataLayerType getDataLayerType(){
		return dataLayerType;
	}
	
	public void cleanUpImage(){
		for (TextView t : squareImages) {
			MapHelper.mapView.removeMarker(t);
		}
		squareImages.clear();
	}
	
	
	public void cleanUpImageForScan(){
		for (TextView t : squareImagesForScan) {
			MapHelper.mapView.removeMarker(t);
		}
		squareImagesForScan.clear();
		gridSquareOrder.clean();
	}
	
	private void addSquareImage(int x,int y,String text) {
		final ImageSquareManager actualInstance = this;
		TextView square = new TextView(activity);
		square.setBackgroundDrawable(BitmapCreator.getSquareBitmapDrawable(BitmapCreator.COLOR_BLUE));
		square.setTag( new int[] { x,y } );
		square.setOnClickListener(new TextView.OnClickListener() {
			@Override
			public void onClick(View v) {
				int[] coordinates = (int[]) v.getTag();
				Static.dialogOnSqaure(activity, scanResultsGrid, coordinates,actualInstance);
				//Static.dialogWithText(activity, scanResultsGrid.getScanResultFromCoordinates(coordinates).getInfo());
			}
			
		});
		if (text != null && !text.matches("")) {
			square.setText(text);
			square.setTextSize(15);
			square.setTextColor(R.color.black);
			square.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
		}
		int[] trimmPosition = MapHelper.gridPositionToTrimPosition(new int[] {x,y});
		MapHelper.mapView.addMarker(square, trimmPosition[0], trimmPosition[1],true);
		squareImages.add(square);
		onScaleChanged(MapHelper.mapView.getScale());
	}
	
	
	private void addSquareImageForScan(int[] trimmedPosition) throws Exception {
		TextView cell = new TextView(activity);
		cell.setBackgroundDrawable(BitmapCreator.getSquareBitmapDrawable(BitmapCreator.COLOR_GREEN));
		cell.setText((squareImagesForScan.size()+1)+"");
		cell.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
		cell.setTextSize(20);
		cell.setTextColor(R.color.black);
		cell.setTag(new Integer(squareImagesForScan.size()+1));
		cell.setOnClickListener(new TextView.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (isCurrentLayerScanLayer()) {
					if ( ((Integer) v.getTag()) == squareImagesForScan.size()) {
						gridSquareOrder.deleteLastSquare();
						MapHelper.mapView.removeMarker(v);
						squareImagesForScan.remove(v);
						((ScanActivity) activity).hideOrShowScanButton();
					}
				}
			}
		});
		MapHelper.mapView.addMarker(cell,trimmedPosition[0] , trimmedPosition[1], true);
		squareImagesForScan.add(cell);
		onScaleChanged(MapHelper.mapView.getScale());
		int[] coordinates = MapHelper.getGridCoordinates(trimmedPosition);
		gridSquareOrder.add(coordinates[0], coordinates[1], squareImagesForScan.size());
	}
	
	

	
	public void onScaleChanged(double scale) {
		for (TextView textview : squareImagesForScan) {
			textview.getLayoutParams().height = (int) ( Constants.CELL_GRID_SIZE_X*scale);
			textview.getLayoutParams().width = (int) ( Constants.CELL_GRID_SIZE_Y*scale);
		}
		for (TextView textview : squareImages) {
			textview.getLayoutParams().height = (int) ( Constants.CELL_GRID_SIZE_X*scale);
			textview.getLayoutParams().width = (int) ( Constants.CELL_GRID_SIZE_Y*scale);
		}
	}
	
	public GridSquareOrder getGridSquareOrder(){
		return gridSquareOrder;
	}
	
	public int getNumberOfNewScanSquares(){
		return squareImagesForScan.size();
	}
	
	
	private Boolean isGoodPositionForAddSquare (int[] trimmedPosition){
		Boolean status = false;
		int[] newGridCoordinates = MapHelper.getGridCoordinates(trimmedPosition);
		int[] currentGridCoordinates = gridSquareOrder.getPositionOfOrderNum(squareImagesForScan.size());
				
		// x+1, y=y
		if(newGridCoordinates[0] == currentGridCoordinates[0]+1 && newGridCoordinates[1] == currentGridCoordinates[1]) {
			status = true;
		}
		// x-1 y=y
		else if(newGridCoordinates[0] == currentGridCoordinates[0]-1 && newGridCoordinates[1] == currentGridCoordinates[1]) {
			status = true;
		}
		// x=x,y+1
		else if(newGridCoordinates[0] == currentGridCoordinates[0] && newGridCoordinates[1] == currentGridCoordinates[1]+1) {
			status = true;
		}
		// x=x, y-1
		else if(newGridCoordinates[0] == currentGridCoordinates[0] && newGridCoordinates[1] == currentGridCoordinates[1]-1) {
			status = true;
		}
		
		// current and new is same
		if(newGridCoordinates[0] == currentGridCoordinates[0] && newGridCoordinates[1] == currentGridCoordinates[1]){
			status = false;
		}
		
		// in position is another view
		if (gridSquareOrder.getOrderNumOfPosition(newGridCoordinates) != 0){
			status = false;
		}
		
		
		return status;
	}

	public Boolean isCurrentLayerScanLayer(){
		return dataLayerType.getDataLayerType().matches(Constants.DATA_LAYER_NONE);
	}
	
}
