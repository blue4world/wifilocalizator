package cz.mendelu.wifilocalizator.other;

import cz.mendelu.wifilocalizator.managers.SettingsManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;

public class BitmapCreator {
	public static final int COLOR_RED = 1;
	public static final int COLOR_GREEN = 2;
	public static final int COLOR_BLUE = 3;
	static BitmapDrawable bitmapRed = null;
	static BitmapDrawable bitmapGreen = null;
	static BitmapDrawable bitmapBlue = null;
	static BitmapDrawable userSizeBitmapRed = null;
	static BitmapDrawable userSizeBitmapGreen = null;
	static BitmapDrawable userSizeBitmapBlue = null;
	
	
	@SuppressWarnings("deprecation")
	public static BitmapDrawable getSquareBitmapDrawable(int color){
		if (COLOR_BLUE == color) {
			if (bitmapBlue == null) {
				bitmapBlue = new BitmapDrawable(getSquareBitmap(BitmapCreator.COLOR_BLUE,1));
			}
			return bitmapBlue;
		}
		else if (COLOR_GREEN == color) {
			if (bitmapGreen == null) {
				bitmapGreen = new BitmapDrawable(getSquareBitmap(BitmapCreator.COLOR_GREEN,1));
			}
			return bitmapGreen;
		}
		else if (COLOR_RED == color) {
			if (bitmapRed == null) {
				bitmapRed = new BitmapDrawable(getSquareBitmap(BitmapCreator.COLOR_RED,1));
			}
			return bitmapRed;
		}
		else {
			return null;
		}
	}
	
	@SuppressWarnings("deprecation")
	public static BitmapDrawable getUserSizeSquareBitmapDrawable(int color){
		if (COLOR_BLUE == color) {
			if (userSizeBitmapBlue == null) {
				userSizeBitmapBlue = new BitmapDrawable(getSquareBitmap(BitmapCreator.COLOR_BLUE,SettingsManager.FIND_POSITION_SIZE_OF_RESULT_MULTIPLE));
			}
			return userSizeBitmapBlue;
		}
		else if (COLOR_GREEN == color) {
			if (userSizeBitmapGreen == null) {
				userSizeBitmapGreen = new BitmapDrawable(getSquareBitmap(BitmapCreator.COLOR_GREEN,SettingsManager.FIND_POSITION_SIZE_OF_RESULT_MULTIPLE));
			}
			return userSizeBitmapGreen;
		}
		else if (COLOR_RED == color) {
			if (userSizeBitmapRed == null) {
				userSizeBitmapRed = new BitmapDrawable(getSquareBitmap(BitmapCreator.COLOR_RED,SettingsManager.FIND_POSITION_SIZE_OF_RESULT_MULTIPLE));
			}
			return userSizeBitmapRed;
		}
		else {
			return null;
		}
	}

	
	private static Bitmap getSquareBitmap(int color,int sizeMultiplicatior){
		Static.log("sizeMultiplicatior is: "+sizeMultiplicatior);
		Bitmap.Config conf = Bitmap.Config.ARGB_8888; // see other conf types
		Bitmap bmp = Bitmap.createBitmap((Constants.CELL_GRID_SIZE_X*sizeMultiplicatior), (Constants.CELL_GRID_SIZE_Y*sizeMultiplicatior), conf); // this creates a MUTABLE bitmap
		for (int i=0; i < bmp.getWidth(); i++)
		{
		for (int j=0; j < bmp.getHeight(); j++)
		 {
			bmp.setPixel(i, j, getColorValue(color));
		 }
		}
		return bmp;
		
	}
	
	
	public static int getColorValue(int color) {
		if (color == COLOR_BLUE) {
			return Color.argb(Constants.SQUARE_BITMAP_ALPHA,0, 0, 100);
		}
		else if (color == COLOR_GREEN) {
			return Color.argb(Constants.SQUARE_BITMAP_ALPHA,0, 100, 0);
		}
		else {
			return Color.argb(Constants.SQUARE_BITMAP_ALPHA,220, 0, 0);
		}
	}
	
	

}
