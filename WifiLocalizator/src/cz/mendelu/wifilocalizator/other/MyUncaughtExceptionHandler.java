package cz.mendelu.wifilocalizator.other;

import java.lang.Thread.UncaughtExceptionHandler;


public class MyUncaughtExceptionHandler implements UncaughtExceptionHandler{

	@Override
	public void uncaughtException(Thread thread, Throwable ex) {
		Static.logExceptionDetail((Exception) ex);
    	System.exit(0);
	}

}
