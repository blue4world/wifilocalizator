package cz.mendelu.wifilocalizator.other;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import android.os.Environment;
import android.util.Xml;
import cz.mendelu.wifilocalizator.dataholers.ScanResult;
import cz.mendelu.wifilocalizator.dataholers.ScanResultAp;
import cz.mendelu.wifilocalizator.dataholers.ScanResultsFloors;
import cz.mendelu.wifilocalizator.dataholers.ScanResultsGrid;
import cz.mendelu.wifilocalizator.managers.SettingsManager;

public class ManageXmlData {
	public static final String tag_root = "wifi_data";
	public static final String attr_timestamp = "timestamp";
	public static final String attr_gridSize = "grid_size";
	public static final String tag_sqaure = "square";
	public static final String tag_wifi = "wifi";
	public static final String tag_ssid = "ssid";
	public static final String tag_bssid = "bssid";
	public static final String tag_level = "level";
	public static final String tag_all_level = "all_levels";
	public static final String attr_cooridnate_x = "coordinate_x";
	public static final String attr_cooridnate_y = "coordinate_y";
	public static final String attr_number_of_scans = "num_of_scans";
	
	
	public static int load(ScanResultsFloors scanResultsFloors) {
			int numOfLoadedFloors = 0;
			for (int i=0; i < Constants.NUMBER_OF_FLOORS; i++) {
				ScanResultsGrid scanResultsGrid = loadXMLFileForFloor(i);
				if (scanResultsGrid != null) {
					scanResultsFloors.addScanResultGrid(scanResultsGrid);
					Static.log("Z XML bylo nacteno podlazi: "+i);
					numOfLoadedFloors++;
				}
				else {
					Static.log("Pro podlazi "+i+" nemam XML data");
				}
			}
		return numOfLoadedFloors;
	
	}
	
	public static Boolean saveFloor(ScanResultsGrid scanResultsGrid,int floor){
		return saveXMLFileForFloor(floor, scanResultsGrid);
	}
	
	public static int saveAll(ScanResultsFloors scanResultsFloors) {
		int numOfSavedFloors = 0;
		for (ScanResultsGrid scanResultsGrid : scanResultsFloors.getAllScanResultsGrids()) {
			Boolean succes = saveXMLFileForFloor(scanResultsGrid.getFloor(), scanResultsGrid);
			if (succes) {
				numOfSavedFloors++;
			}
		}
		
		return numOfSavedFloors;
	}
	
	private static Boolean saveXMLFileForFloor(int floor,ScanResultsGrid scanResultsGrid) {
		Boolean succes = false;
		try {
			// file for save
			File sdCard = Environment.getExternalStorageDirectory();
			File dir = new File (sdCard.getAbsolutePath() + Constants.XML_DATA_DIR);
			dir.mkdirs();
			File file = new File(dir, Constants.XML_DATA_FILENAME+floor+Constants.XML_DATA_SUFFIX);
			FileOutputStream fos = new FileOutputStream(file);
			
			// parse
		    XmlSerializer serializer = Xml.newSerializer();
		    serializer.setOutput(fos, "UTF-8");
		    serializer.startDocument(null, Boolean.valueOf(true));
		    serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
		    serializer.startTag(null, tag_root);
		    serializer.attribute(null, attr_timestamp, String.valueOf(System.currentTimeMillis()));
		    serializer.attribute(null, attr_gridSize, String.valueOf(SettingsManager.GRID_SIZE));
		    
			for(int x = 0; x < SettingsManager.GRID_SIZE; x++) {
				for(int y = 0; y < SettingsManager.GRID_SIZE; y++) {
					if (! scanResultsGrid.isPositionEmpty(x, y) ) {
						ScanResult scanResult = scanResultsGrid.getScanResultFromCoordinates(new int[] { x ,y });
						serializer.startTag(null, tag_sqaure);
						serializer.attribute(null, attr_cooridnate_x, String.valueOf(x));
						serializer.attribute(null, attr_cooridnate_y, String.valueOf(y));
						
							for (ScanResultAp ap : scanResult.getListResult()) {
								// start new AP
								serializer.startTag(null, tag_wifi);
								// bssid
								serializer.startTag(null, tag_bssid);
								serializer.text(ap.getBssid());
								serializer.endTag(null, tag_bssid);
								
								// ssid
								serializer.startTag(null, tag_ssid);
								serializer.text(ap.getSsid());
								serializer.endTag(null, tag_ssid);
								
								// level
								serializer.startTag(null, tag_level);
								serializer.text(String.valueOf(ap.getLevel()));
								serializer.endTag(null, tag_level);
								
								// all levels
								serializer.startTag(null, tag_all_level);
								serializer.text(ap.getAllLevels());
								serializer.endTag(null, tag_all_level);
								
								// end AP
								serializer.endTag(null, tag_wifi);
							}
						serializer.endTag(null, tag_sqaure);
					}
				}
			}
		    
			serializer.endTag(null, tag_root);
			serializer.endDocument();
			serializer.flush();
		    fos.close();
		    succes = true;
		}
		catch (Exception e){
			//Static.logExceptionDetail(e);
			succes = false;
		}
		return succes;
		
	}
	
	
	
	private static ScanResultsGrid loadXMLFileForFloor(int floor) {
		try {
			ScanResultsGrid scanResultsGrid = new ScanResultsGrid(floor);
			
			// load and test file
			File sdCard = Environment.getExternalStorageDirectory();
			File dir = new File (sdCard.getAbsolutePath() + Constants.XML_DATA_DIR);
			File file = new File(dir, Constants.XML_DATA_FILENAME+floor+Constants.XML_DATA_SUFFIX);
			if (!file.exists() ) {
				throw new Exception(Constants.EXC_xmlFileNotFound);
			}
			
			// load data from file
			FileInputStream fis = new FileInputStream(file);
			InputStreamReader isr = null;
			isr = new InputStreamReader(fis);
			char[] inputBuffer = new char[fis.available()];
			isr.read(inputBuffer);
			String data = new String(inputBuffer);
			isr.close();
			fis.close();
			
			// test if file is not empty
			if(data != null && !data.matches("")) {
			
				InputStream is = new ByteArrayInputStream(data.getBytes("UTF-8"));
				
				XmlPullParserFactory xmlFactoryObject = XmlPullParserFactory.newInstance();
				XmlPullParser myParser = xmlFactoryObject.newPullParser();
				  
				myParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
				myParser.setInput(is,"UTF-8");
				      
				      
				  String text=null;
				  int event = myParser.getEventType();
				  ScanResult scanResult = new ScanResult();
				  ScanResultAp scanResultAp = new ScanResultAp();
				  int x = 0;
				  int y = 0;
				         while (event != XmlPullParser.END_DOCUMENT) {
				            String name=myParser.getName();
				            switch (event){
				               case XmlPullParser.START_TAG:
				            	   if(name.equals(tag_root)) {
				            		   scanResultsGrid.setTimestamp(myParser.getAttributeValue(null,attr_timestamp));
				            		   scanResultsGrid.setGridSize(Integer.valueOf(myParser.getAttributeValue(null, attr_gridSize)));
				            	   }
				            	   else if(name.equals(tag_sqaure)){ 	
					                	  x = Integer.parseInt(myParser.getAttributeValue(null, attr_cooridnate_x));
					                	  y = Integer.parseInt(myParser.getAttributeValue(null, attr_cooridnate_y));
					                	  scanResult = new ScanResult();
					               }
				            	   else if(name.equals(tag_wifi)){ 	
					                	 scanResultAp = new ScanResultAp();
					               }
				               break;
				               
				               case XmlPullParser.TEXT:
				            	   text = myParser.getText();
				               break;
				               
				               case XmlPullParser.END_TAG:
				                  if(name.equals(tag_bssid)){
				                	  	 scanResultAp.setBssid(text);
				                  }
				                  else if(name.equals(tag_ssid)){
				                	  	 scanResultAp.setSsid(text);
				                  }
				                  else if(name.equals(tag_level)){
				                	  	 scanResultAp.setLevel(Integer.parseInt(text));
				                  }
				                  else if(name.equals(tag_all_level)) {
				                	  	 scanResultAp.setAllLevels(text);
				                  }
				                  else if(name.equals(tag_wifi)){
				                	  	 scanResult.addForceNoSort(scanResultAp);
				                  }
				                  else if(name.equals(tag_sqaure)){
				                	  	 scanResultsGrid.add(x, y, scanResult);
				                  }
				                  
				                  break;
				                  }		 
				                  event = myParser.next(); 
				
				              }
			    }
			    else {
			    	throw new Exception(Constants.EXC_xmlFileIsEmpty);
			    }
			    
			    return scanResultsGrid;
		}
		catch (Exception e){
			//Static.logExceptionDetail(e);
			return null;
		}
	}


}
