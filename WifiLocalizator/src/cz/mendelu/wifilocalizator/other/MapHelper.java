package cz.mendelu.wifilocalizator.other;

import com.qozix.mapview.MapView;


public class MapHelper {
	public static MapView mapView;
    public static int currentFloor = 0;
	
	public static void setMapView(MapView mapView) {
		MapHelper.mapView = mapView;
	}
	
	public static double[] relativePositionToGps(int x,int y) {
		int[] position = relativePositionToAbsolute(x, y);
		double[] gpsPosition = mapView.pixelsToLatLng(position[0], position[1]);
		return gpsPosition;
	}
	
	public static int[] relativePositionToAbsolute(int x,int y){
		double actualScale = mapView.getScale();
		int[] absolute = new int[2];
		absolute[0] = (int) ( x / actualScale );
		absolute[1] = (int) ( y / actualScale );
		return absolute;
	}
	
	/*
	public static int[] relativePositionToAbsolute(int x,int y){
		double actualScale = mapView.getScale();
		int[] absoluteScale = new int[2];
		if (actualScale == 1) {
			absoluteScale[0] = x; 
			absoluteScale[1] = y;		
		}
		else {
			absoluteScale[0] = (int)(x * ((1-actualScale)+1));
			absoluteScale[1] = (int)(y * ((1-actualScale)+1));
		}
		
		return absoluteScale;
	}
	*/
	public static int[] getGridCoordinates(int[] trimmedPosition) {
		int[] gridCoordinates = new int[2];
		gridCoordinates[0] = trimmedPosition[0] / Constants.CELL_GRID_SIZE_X;
		gridCoordinates[1] = trimmedPosition[1] / Constants.CELL_GRID_SIZE_Y;
		return gridCoordinates;
	}
	
	public static int[] trimPositionToGrid(int[] absolutePosition){
		int[] newPosition = new int[2];
		
		int x = absolutePosition[0];
		int y = absolutePosition[1];
		
		int xDiv = x / Constants.CELL_GRID_SIZE_X;
		int yDiv = y / Constants.CELL_GRID_SIZE_Y;
		
		newPosition[0] = xDiv*Constants.CELL_GRID_SIZE_X;
		newPosition[1] = yDiv*Constants.CELL_GRID_SIZE_Y;
		
		return newPosition;
	}
	
	public static int[] gridPositionToTrimPosition(int[] gridPosition) {
		int[] trimPosition = new int[2];
		trimPosition[0] = gridPosition[0] * Constants.CELL_GRID_SIZE_X;
		trimPosition[1] = gridPosition[1] * Constants.CELL_GRID_SIZE_Y;
		return trimPosition;
	}
	
	
	public static Boolean isZoomedToMax(){
		if (mapView.getScale() == 1){
			return true;
		}
		else {
			return false;
		}
	}

}
