package cz.mendelu.wifilocalizator.other;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;
import cz.mendelu.wifilocalizator.LocalizeActivity;
import cz.mendelu.wifilocalizator.R;
import cz.mendelu.wifilocalizator.ScanActivity;
import cz.mendelu.wifilocalizator.dataholers.FindResult;
import cz.mendelu.wifilocalizator.dataholers.ScanResultsGrid;
import cz.mendelu.wifilocalizator.managers.ImageSquareManager;


public class Static {
	
	public static WakeLock takeWakeLock(Context context){
		WakeLock wl;
		PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        wl = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "DoNjfdhotDimScreen");
        return wl;
	}
	
	
	public static void showToast(Context context,String msg){
		try {
			Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
		}
		catch (Exception e){
			Static.logExceptionDetail(e);
		}
	}
	
	public static void showToast(Context context,String msg,int lenght){
		try {
			if (lenght == Toast.LENGTH_LONG) {
				Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
			}
			else {
				Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
			}
		}
		catch (Exception e){
			Static.logExceptionDetail(e);
		}
	}
	
	public static void log(String msg){
		if (Constants.DEBUG_MSG) {
			String methodName = "";
			try {
				methodName = Thread.currentThread().getStackTrace()[3].getMethodName();
			}
			catch (Exception e){
				Static.logExceptionDetail(e);
			}
			if (!methodName.matches("")) {
				methodName += ": ";
			}
			Log.v(Constants.LOG,methodName+msg);
		}
	}
		
	public static void logExceptionDetail(Exception e){
		if (Constants.DEBUG_MSG) {
			Log.v(Constants.LOG,"--POZOR Vyjimka --");
			int i = 0;
			for (StackTraceElement tmp : e.getStackTrace()){
				Log.v(Constants.LOG,"Soubor: "+tmp.getFileName());
				Log.v(Constants.LOG,"Trida: "+tmp.getClassName());
				Log.v(Constants.LOG,"Metoda: "+tmp.getMethodName());
				Log.v(Constants.LOG,"Radek: "+tmp.getLineNumber());
				Log.v(Constants.LOG,"---");
				if (i >= 4) {
					break;
				}
				i++;
			}
			
			Log.v(Constants.LOG,"getClass: "+e.getClass());
			Log.v(Constants.LOG,"getMessage:"+e.getMessage());
			Log.v(Constants.LOG,"getCause: "+e.getCause());
			Log.v(Constants.LOG,"Thread: "+Thread.currentThread().getName());
			Log.v(Constants.LOG,"----------------");
		}
		
	}
	
	public static void dialogWithText(Context context,String text) {
		try {
			AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
			alertDialog.setMessage(text);
			alertDialog.setPositiveButton(context.getResources().getString(R.string.ok), null);
			alertDialog.show();
		}
		catch (Exception e){
			Static.logExceptionDetail(e);
		}
	}
	
	public static void dialogOnSqaure(final Context context,final ScanResultsGrid scanResultsGrid, final int[] coordinates,final ImageSquareManager squareManager){
		AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
		String message = context.getString(R.string.square)+" "+coordinates[0]+":"+coordinates[1]+"\n\n";
		try {
			message += "Počet AP: \n"+scanResultsGrid.getScanResultGrid()[coordinates[0]][coordinates[1]].getListResult().size()+"\n";
			message += "Průměrná směrodatná odchylka: \n"+scanResultsGrid.getScanResultGrid()[coordinates[0]][coordinates[1]].getAverageOfStandartDeviation()+"\n";
		}
		catch (Exception e){
			Static.logExceptionDetail(e);
		}
		alertDialog.setMessage(message);
		alertDialog.setPositiveButton(context.getString(R.string.back), null);
		alertDialog.setNeutralButton(context.getString(R.string.info), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				dialogWithText(context, scanResultsGrid.getScanResultFromCoordinates(coordinates).getInfo());
			}
		});
		alertDialog.setNegativeButton(context.getString(R.string.delete), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				Static.showToast(context, context.getString(R.string.square_data_was_deleted));
				scanResultsGrid.deleteScanResultFromCoordinates(coordinates);
				squareManager.reloadDataLayer();
				ManageXmlData.saveFloor(scanResultsGrid, MapHelper.currentFloor);
				
			}
		});
		alertDialog.show();
	}
	
	
	public static Boolean isApiGreater11(){
		return (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB);
	}
	
	public static Boolean isApiGreater13(){
		return (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB_MR2);
	}
	
	public static Boolean isApiGreater10(){
		return (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.GINGERBREAD_MR1);
	}
	
	public static void handleExceptionDialog(Exception e,final Context context){
		try {
			//String errorString = "";
			if (e != null) {
				if (e.getMessage() == null) {
					Static.log("Nemohu zobrazit hlasku o chybe protoze getMessage je null");
				}
				else {
					Static.dialogWithText(context, e.getMessage());
				}
			}
			else {
				Static.log("Vyjimka je null");
			}
		}
		catch (Exception d){
			Static.logExceptionDetail(d);
		}
	}
	
	public static void enableWifi(Context context) {
        WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        if (wifi.isWifiEnabled() == false)
        {
        	Static.showToast(context, context.getString(R.string.enabling_wifi));
            wifi.setWifiEnabled(true);
        } 
	}
	
	public static void hideTitleBar(ActionBarActivity activity){
		try {
			activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		}
		catch (Exception e){
			Static.logExceptionDetail(e);
		}
	}
	
	public static Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap, final boolean order)
    {

        List<Map.Entry<String, Integer>> list = new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

        // Sorting the list based on values
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>()
        {
            public int compare(Map.Entry<String, Integer> o1,
                    Map.Entry<String, Integer> o2)
            {
                if (order)
                {
                    return o1.getValue().compareTo(o2.getValue());
                }
                else
                {
                    return o2.getValue().compareTo(o1.getValue());

                }
            }
        });

        // Maintaining insertion order with the help of LinkedList
        Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
        for (Map.Entry<String, Integer> entry : list)
        {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }

	
	public static void goToScanActivity(Activity activity) {
		activity.startActivity(new Intent(activity,ScanActivity.class));
	}
	
	public static void goToLocalizeActivity(Activity activity) {
		activity.startActivity(new Intent(activity,LocalizeActivity.class));
	}
	
	public static String[] getFloorsArray(Context context){
		String[] floors = new String[Constants.NUMBER_OF_FLOORS];
		for (int i=0; i < floors.length; i++){
			floors[i] = context.getString(R.string.floor)+" "+i;
		}
		return floors;
	}
	
	public static void deleteDataForNeuralFromFile(){
		File sdCard = Environment.getExternalStorageDirectory();
		File dir = new File (sdCard.getAbsolutePath() + Constants.XML_DATA_DIR);
		File file = new File(dir, "data_for_neural.txt");
		file.delete();
	}
	
	public static void saveDataForNeuralToFile(String data){
		try {
			// test and create directory
			File sdCard = Environment.getExternalStorageDirectory();
			File dir = new File (sdCard.getAbsolutePath() + Constants.XML_DATA_DIR);
			dir.mkdirs();
			// file
			File file = new File(dir, "neural_network_log.txt");
		    file.createNewFile();
			
		    FileWriter writer = new FileWriter(file,true);
	        BufferedWriter out = new BufferedWriter(writer);
	        out.write(data);
	        out.close();
		}
		catch (Exception e){
			Static.logExceptionDetail(e);
		}
	}
	
	public static void saveDataResultsToFile(String data){
		try {
			// test and create directory
			File sdCard = Environment.getExternalStorageDirectory();
			File dir = new File (sdCard.getAbsolutePath() + Constants.XML_DATA_DIR);
			dir.mkdirs();
			// file
			File file = new File(dir, "results.txt");
		    file.createNewFile();
			
		    FileWriter writer = new FileWriter(file,true);
	        BufferedWriter out = new BufferedWriter(writer);
	        out.write(data);
	        out.close();
		}
		catch (Exception e){
			Static.logExceptionDetail(e);
		}
	}
	
	public static String convertStreamToString(InputStream is) throws Exception {
	    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	    StringBuilder sb = new StringBuilder();
	    String line = null;
	    while ((line = reader.readLine()) != null) {
	      sb.append(line).append("\n");
	    }
	    reader.close();
	    return sb.toString();
	}
	
	
}
