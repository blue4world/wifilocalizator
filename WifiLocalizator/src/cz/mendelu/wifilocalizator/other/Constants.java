package cz.mendelu.wifilocalizator.other;

import cz.mendelu.wifilocalizator.managers.SettingsManager;
import android.R.string;

public class Constants {
	public static final String LOG = "WifiLocalizator";

	public static final Boolean DEBUG_MSG = true;
	public static final Boolean ENABLE_SAVING_LOCATION_RESULT = true;
	public static final Boolean DEBUG_MSG_WIFI_SCAN = false;
	public static final double START_SCALE = 0.8;
	public static final int NUMBER_OF_FLOORS = 5;
	
	// Activity ID
	public static final int NO_ACTIVITY = 0;
	public static final int SCANNER_ACTIVITY = 1;
	public static final int LOCALIZE_ACTIVITY = 2;
	
	// FILES
	public static final String XML_DATA_FILENAME = "data_";
	public static final String XML_DATA_SUFFIX = ".xml";
	public static final String XML_DATA_DIR = "/wifiLocalizationData";
	
	public static final String XML_NEURAL_DATA_DIR = "/neural_data";
	public static final String XML_NEURAL_DATA_SUFFIX = ".enc";
	// DATA LAYER 
	public static final String DATA_LAYER_ALL = "Všechny";
	public static final String DATA_LAYER_NONE = "Scanovat";
	
	// GUI
	public static final int SQUARE_BITMAP_ALPHA = 125;
	
	// GPS 
	public static final int GPS_COORDINATE_LEFT = 10;
    public static final int GPS_COORDINATE_RIGHT = 0;
    public static final int GPS_COORDINATE_TOP = 10;
    public static final int GPS_COORDINATE_BOTTOM = 0;
 
    // image constants
    public static final int MAP_MAX_RESOLUTION_X = 1200;
    public static final int MAP_MAX_RESOLUTION_Y = 1200;
    
    // grid constants
    //public static final int NUMBER_OF_CELLS_IN_GRID = 10;
    
    public static int NUMBER_OF_CELLS_IN_GRID_POWER = (int) Math.pow(SettingsManager.GRID_SIZE, 2);
    public static int CELL_GRID_SIZE_X = MAP_MAX_RESOLUTION_X / SettingsManager.GRID_SIZE;
    public static int CELL_GRID_SIZE_Y = MAP_MAX_RESOLUTION_Y / SettingsManager.GRID_SIZE;
    
    // user size bitmap
    // public static final int USER_SIZE_SQUARE_BITMAP_SIZE = 2;
    
    // faro-free exception
    //public static final Boolean SCANNER_DONT_SCAN_FARO_FREE = true;
    //public static final String FARO_FREE_SSID = "faro-free";
    
    //public static final Boolean SCANNER_SCAN_ONLY_EDUROAM = true;
    public static final String ERUDOAM_SSID = "eduroam";
    
    // messages
    public static final String MSG_x = "MSG_x";
    public static final String MSG_y = "MSG_y";
    public static final String MSG_fitness = "MSG_fitness";
    public static final String MSG_numOfPosition = "MSG_numOfPosition";
    public static final String MSG_floor = "MSG_floor";
    
    // Exceptions
    public static final String EXC_notAbleToScanWifiNetwork = "EXC_notAbleToScanWifiNetwork";
    public static final String EXC_classDontImplementMapUpdatedInterface = "EXC_ClassDontImplementMapUpdatedInterface";
    public static final String EXC_numberOfSquaresIsZero = "EXC_numberOfSquaresIsZero";
    public static final String EXC_numOfScanInScanResultIsZero = "EXC_numOfScanInScanResultIsZero";
    public static final String EXC_xmlFileNotFound = "EXC_xmlFileNotFound";
    public static final String EXC_xmlFileIsEmpty = "EXC_xmlFileIsEmpty";
    public static final String EXC_testHolderEmpty = "EXC_testHolderEmpty";
    public static final String EXC_gridSquareNumberIsNotSameAsListOfScannedResult = "EXC_gridSquareNumberIsNotSameAsListOfScannedResult";
    public static final String EXC_positionIsBiggerThanOrderGridSize = "EXC_positionIsBiggerThanOrderGridSize";
}



