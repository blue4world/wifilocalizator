package cz.mendelu.wifilocalizator.wififinder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cz.mendelu.wifilocalizator.dataholers.FindResutlHolder;
import cz.mendelu.wifilocalizator.dataholers.ScanResult;
import cz.mendelu.wifilocalizator.dataholers.ScanResultAp;
import cz.mendelu.wifilocalizator.dataholers.ScanResultsFloors;
import cz.mendelu.wifilocalizator.dataholers.ScanResultsGrid;
import cz.mendelu.wifilocalizator.managers.SettingsManager;

public class FindPositionEucledianDistance {
	static final int LEVEL_FROM_AP_WHICH_IS_NOT_IN_GRID = 100;  // must be possitive + value
	static double SHIFT_RANGE_MIN = -6; // old -4
	static double SHIFT_RANGE_MAX = 6; // old 4
	static final int THRESHOLD = 30; // from 10dB to 30dB.
	static final double SHIFT_STEP = 1;
	private static List<Integer> fitnessArray = new ArrayList<Integer>();
	
	public static FindResutlHolder findPosition(ScanResult dataFromScan,ScanResultsFloors scanResultsFloors,Boolean useShift) {
		List<FindResutlHolder> findResutlHolders = new ArrayList<FindResutlHolder>();
		
		SHIFT_RANGE_MIN = (-SettingsManager.SHIFT_SIZE); 
		SHIFT_RANGE_MAX = SettingsManager.SHIFT_SIZE;
		
		for (ScanResultsGrid scanResultsGrid : scanResultsFloors.getAllScanResultsGrids()) {
			FindResutlHolder fr = findPositionForFloor(dataFromScan, scanResultsGrid, scanResultsGrid.getFloor(),useShift);
			findResutlHolders.add(fr);
		}
		
		return FindPosition.getBestFindResultHolder(findResutlHolders);
	}	
	
	private static FindResutlHolder findPositionForFloor(ScanResult dataFromScan,ScanResultsGrid scanResultsGridObj,int floor,Boolean useShift){
		FindResutlHolder findResutlHolder = new FindResutlHolder();
		findResutlHolder.setFloor(floor);
		
		ScanResult[][] scanResultsGrid = scanResultsGridObj.getScanResultGrid();
		
		int[][] fitnessGrid = FindPosition.createFitnessGrid();
		
		for(int x = 0; x < SettingsManager.GRID_SIZE; x++) {
			for(int y = 0; y < SettingsManager.GRID_SIZE; y++) {
				if (scanResultsGrid[x][y] != null) {
					fitnessGrid[x][y] = calculateFitness(scanResultsGrid[x][y], dataFromScan,useShift);
				}
			}
		}
		
		findResutlHolder.addFindResults(fitnessGrid);
		
		
		return findResutlHolder;
	}
	
	private static int calculateFitness(ScanResult dataFromGrid,ScanResult dataFromScan,Boolean useShift){
		int fitness;
		
		if (!useShift) {
			fitness = calculateFitnessForShift(dataFromGrid,dataFromScan,0,false);
		}
		else {
			fitnessArray.clear();
			for (double i=SHIFT_RANGE_MIN;i<=SHIFT_RANGE_MAX;i=i+SHIFT_STEP) {
				fitnessArray.add(calculateFitnessForShift(dataFromGrid, dataFromScan, i,true));
			}
			fitness = Collections.min(fitnessArray).intValue();
		}
		fitness = (int) Math.round(Math.sqrt(fitness));
		
		return fitness;
	}

	
	private static int calculateFitnessForShift(ScanResult dataFromGrid,ScanResult dataFromScan,double shift,Boolean enableThreshold){
		int fitness = 0;
		
		for (ScanResultAp ap: dataFromScan.getListResult()) {
			int levelFromGrid = dataFromGrid.getLevelOfBssid(ap.getBssid());
			int levelFromScan = ap.getLevel();
			int computedValue;
			if (levelFromGrid != 0) {
				computedValue =  (int) Math.abs( Math.abs(levelFromScan-levelFromGrid) + shift );
			}
			else {
				computedValue = LEVEL_FROM_AP_WHICH_IS_NOT_IN_GRID;
			}
			
			// Threshold
			if (enableThreshold) {
				if (computedValue > THRESHOLD) {
					computedValue = THRESHOLD;
				}
			}
			
			fitness += Math.pow(computedValue, 2);
			
		}
		
	
		return fitness;
	}

		
}


