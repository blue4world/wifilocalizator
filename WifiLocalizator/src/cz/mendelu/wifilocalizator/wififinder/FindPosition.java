package cz.mendelu.wifilocalizator.wififinder;

import java.util.Comparator;
import java.util.List;

import cz.mendelu.wifilocalizator.dataholers.FindResult;
import cz.mendelu.wifilocalizator.dataholers.FindResutlHolder;
import cz.mendelu.wifilocalizator.dataholers.ScanResult;
import cz.mendelu.wifilocalizator.managers.SettingsManager;
import cz.mendelu.wifilocalizator.other.Constants;
import cz.mendelu.wifilocalizator.other.Static;

public class FindPosition {
	
	public static ScanResult getDataFromScan(ScanResult scanResult){
		ScanResult dataFromScan = new ScanResult();
		try {
			dataFromScan.addWithAverage(scanResult, SettingsManager.FIND_POSITION_NUMBER_OF_ACCEPTED_AP);
		}
		catch (Exception e){
			Static.logExceptionDetail(e);
		}
		return dataFromScan;
	}
	
	
	public static int[][] createFitnessGrid(){
		int[][]  fitnessGrid = new int[SettingsManager.GRID_SIZE][SettingsManager.GRID_SIZE];
		for(int x = 0; x < SettingsManager.GRID_SIZE; x++) {
			for(int y = 0; y < SettingsManager.GRID_SIZE; y++) {
				fitnessGrid[x][y] = Integer.MAX_VALUE;
			}
		}
		return fitnessGrid;
	}
	
	public static int[][] createFitnessGridFillWithZeros(){
		int[][]  fitnessGrid = new int[SettingsManager.GRID_SIZE][SettingsManager.GRID_SIZE];
		for(int x = 0; x < SettingsManager.GRID_SIZE; x++) {
			for(int y = 0; y < SettingsManager.GRID_SIZE; y++) {
				fitnessGrid[x][y] = 0;
			}
		}
		return fitnessGrid;
	}
	
	public static int[][] reverseFitnessGridZerosToIntMap(int[][] fitnessGrid){
		for(int x = 0; x < SettingsManager.GRID_SIZE; x++) {
			for(int y = 0; y < SettingsManager.GRID_SIZE; y++) {
				if (fitnessGrid[x][y] == 0){
					fitnessGrid[x][y] = Integer.MAX_VALUE;
				}
			}
		}
		return fitnessGrid;
	}
	
	
	
	public static void printDataFromWifiScan(ScanResult dataFromScan){
		Static.log("Pro hledani pozice pouzivam data: \n"+dataFromScan.getInfo());
	}
	
	public static FindResutlHolder getBestFindResultHolder(List<FindResutlHolder> findResultHolders) {
		FindResutlHolder bestFindResultHolder = new FindResutlHolder();
		for (FindResutlHolder fr : findResultHolders) {
			if (fr.getBestFitnessValue() < bestFindResultHolder.getBestFitnessValue()) {
				bestFindResultHolder = fr;
			}
		}
		return bestFindResultHolder;
	}
	
	
	public class FindResultComparator implements Comparator<FindResult> {
		
		@Override
		public int compare(FindResult arg0, FindResult arg1) {
			
			if (arg0.getFitness() > arg1.getFitness()) {
				return 1;
			}
			else if (arg0.getFitness() < arg1.getFitness()){
				return -1;
			}
			else {
				return 0;
			}
			
		}
	}
	
	

}
