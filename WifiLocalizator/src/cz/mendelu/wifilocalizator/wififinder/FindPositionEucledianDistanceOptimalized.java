package cz.mendelu.wifilocalizator.wififinder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import cz.mendelu.wifilocalizator.dataholers.FindResutlHolder;
import cz.mendelu.wifilocalizator.dataholers.ScanResult;
import cz.mendelu.wifilocalizator.dataholers.ScanResultAp;
import cz.mendelu.wifilocalizator.dataholers.ScanResultsFloors;
import cz.mendelu.wifilocalizator.dataholers.ScanResultsGrid;
import cz.mendelu.wifilocalizator.managers.SettingsManager;
import cz.mendelu.wifilocalizator.other.Static;

public class FindPositionEucledianDistanceOptimalized {
	static final int LEVEL_FROM_AP_WHICH_IS_NOT_IN_GRID = 100;  // must be possitive + value
	static int SHIFT_RANGE_MIN = -6; // old -4
	static int SHIFT_RANGE_MAX = 6; // old 4
	static final int THRESHOLD = 30; // from 10dB to 30dB.
	static final int SHIFT_STEP = 1;
	private static List<Integer> fitnessArray = new ArrayList<Integer>();
	
	public static FindResutlHolder findPosition(ScanResult dataFromScan,ScanResultsFloors scanResultsFloors,Boolean useShift) {
		List<FindResutlHolder> findResutlHolders = new ArrayList<FindResutlHolder>();
		
		SHIFT_RANGE_MIN = (-SettingsManager.SHIFT_SIZE); 
		SHIFT_RANGE_MAX = SettingsManager.SHIFT_SIZE;
		
		for (ScanResultsGrid scanResultsGrid : scanResultsFloors.getAllScanResultsGrids()) {
			FindResutlHolder fr = findPositionForFloor(dataFromScan, scanResultsGrid, scanResultsGrid.getFloor(),useShift);
			findResutlHolders.add(fr);
		}
		
		return FindPosition.getBestFindResultHolder(findResutlHolders);
	}	
	
	private static FindResutlHolder findPositionForFloor(ScanResult dataFromScan,ScanResultsGrid scanResultsGridObj,int floor,Boolean useShift){
		FindResutlHolder findResutlHolder = new FindResutlHolder();
		findResutlHolder.setFloor(floor);
		
		ScanResult[][] scanResultsGrid = scanResultsGridObj.getScanResultGrid();
		
		int[][] fitnessGrid = FindPosition.createFitnessGrid();
		
		for(int x = 0; x < SettingsManager.GRID_SIZE; x++) {
			for(int y = 0; y < SettingsManager.GRID_SIZE; y++) {
				if (scanResultsGrid[x][y] != null) {
					fitnessGrid[x][y] = calculateFitness(scanResultsGrid[x][y], dataFromScan);
				}
			}
		}
		
		int averageFitness = cutWorstResult(60, fitnessGrid);
		
		for(int x = 0; x < SettingsManager.GRID_SIZE; x++) {
			for(int y = 0; y < SettingsManager.GRID_SIZE; y++) {
				if (fitnessGrid[x][y] != Integer.MAX_VALUE) {
					
					if (fitnessGrid[x][y] < averageFitness) {
						fitnessArray.clear();
						// add for i = 0 - no shift
						fitnessArray.add(fitnessGrid[x][y]);
						
						for (int i=SHIFT_RANGE_MIN;i<=SHIFT_RANGE_MAX;i=i+SHIFT_STEP) {
							if (i != 0) {
								int fitness = calculateFitnessForShift(scanResultsGrid[x][y], dataFromScan, i,true);
								fitnessArray.add(fitness);
							}
						}
						int bestFitness = Collections.min(fitnessArray).intValue();
						
						fitnessGrid[x][y] = (int) Math.sqrt(bestFitness);
					}
					else {
						fitnessGrid[x][y] = (int) Math.sqrt(fitnessGrid[x][y]);
					}
					
				}
			}
		}
		
		
		
		findResutlHolder.addFindResults(fitnessGrid);
		
		return findResutlHolder;
	}
	
	private static int calculateFitness(ScanResult dataFromGrid,ScanResult dataFromScan){
		return calculateFitnessForShift(dataFromGrid,dataFromScan,0,false);
	}

	
	private static int calculateFitnessForShift(ScanResult dataFromGrid,ScanResult dataFromScan,int shift,Boolean enableThreshold){
		int fitness = 0;
		Boolean hasRun = false;
		
		for (ScanResultAp ap: dataFromScan.getListResult()) {
			int levelFromGrid = dataFromGrid.getLevelOfBssid(ap.getBssid());
			int levelFromScan = ap.getLevel();
			int computedValue;
			if (levelFromGrid != 0) {
				hasRun = true;
				computedValue =  (levelFromScan-levelFromGrid) + shift;
			}
			else {
				computedValue = LEVEL_FROM_AP_WHICH_IS_NOT_IN_GRID;
			}
			
			// Threshold
			if (enableThreshold) {
				if (computedValue > THRESHOLD) {
					computedValue = THRESHOLD;
				}
			}
			
			fitness += Math.pow(computedValue, 2);
		}
		
		if(!hasRun) {
			fitness = Integer.MAX_VALUE;
		}
	
		return fitness;
	}
	
	private static int getAverageFitness(int[][] fitnessGrid) {
		int sum = 0;
		int count = 0;
		for(int x = 0; x < SettingsManager.GRID_SIZE; x++) {
			for(int y = 0; y < SettingsManager.GRID_SIZE; y++) {
				if (fitnessGrid[x][y] != Integer.MAX_VALUE) {
					sum += fitnessGrid[x][y];
					count++;
				}
			}
		}
		
		if (count != 0){
			return (sum / count); 
		}
		else{
			return Integer.MAX_VALUE;
		}
		
	}
	
	private static int cutWorstResult(int percent,int[][] fitnessGrid){
		Set<Integer> results = new HashSet<Integer>();
		
		for(int x = 0; x < SettingsManager.GRID_SIZE; x++) {
			for(int y = 0; y < SettingsManager.GRID_SIZE; y++) {
				if (fitnessGrid[x][y] != Integer.MAX_VALUE) {
					results.add(fitnessGrid[x][y]);
				}
			}
		}
		
		List<Integer> list = new ArrayList<Integer>(results);
		Collections.sort(list);
		
		int listSize = list.size();
		int cropToPosition = (int)Math.round( listSize / (100.0/(100-percent))  );
		//Static.log("listsize: "+listSize);
		//Static.log("position to crop: "+cropToPosition);
		int finalPos = cropToPosition+1;
		if (finalPos >= list.size()) {
			finalPos = list.size()-1;
		}
		if(finalPos < 0){
			finalPos = 0;
		}
		
		return list.get(finalPos);
		
	}

		
}


