package cz.mendelu.wifilocalizator.wififinder;

import java.util.ArrayList;
import java.util.List;

import cz.mendelu.wifilocalizator.dataholers.FindResutlHolder;
import cz.mendelu.wifilocalizator.dataholers.ScanResult;
import cz.mendelu.wifilocalizator.dataholers.ScanResultAp;
import cz.mendelu.wifilocalizator.dataholers.ScanResultsFloors;
import cz.mendelu.wifilocalizator.dataholers.ScanResultsGrid;
import cz.mendelu.wifilocalizator.managers.SettingsManager;

public class FindPositionApOrder {
	
	public static FindResutlHolder findPosition(ScanResult dataFromScan,ScanResultsFloors scanResultsFloors) {
		List<FindResutlHolder> findResutlHolders = new ArrayList<FindResutlHolder>();
		
		for (ScanResultsGrid scanResultsGrid : scanResultsFloors.getAllScanResultsGrids()) {
			FindResutlHolder fr = findPositionForFloor(dataFromScan, scanResultsGrid, scanResultsGrid.getFloor());
			findResutlHolders.add(fr);
		}
		
		return FindPosition.getBestFindResultHolder(findResutlHolders);

	}	

	
	private static FindResutlHolder findPositionForFloor(ScanResult dataFromScan,ScanResultsGrid scanResultsGridObj,int floor){
		FindResutlHolder findResutlHolder = new FindResutlHolder();
		findResutlHolder.setFloor(floor);
		ScanResult[][] scanResultsGrid = scanResultsGridObj.getScanResultGrid();
		
		for(int x = 0; x < SettingsManager.GRID_SIZE; x++) {
			for(int y = 0; y < SettingsManager.GRID_SIZE; y++) {
				if (scanResultsGrid[x][y] != null) {
					ScanResult dataFromGrid = scanResultsGrid[x][y];
					findResutlHolder.addFindResult(calculateFitness(dataFromGrid, dataFromScan),x,y);
				}
			}
		}
		
		return findResutlHolder;
	}
	
	
	
	
	private static int calculateFitness(ScanResult dataFromGrid,ScanResult dataFromScan){
		int fitness = 0;
		Boolean wasRun = false;
		List<ScanResultAp> listDataFromScan = dataFromScan.getListResult();
		List<ScanResultAp> listDataFromGrid = dataFromGrid.getListResult();
		
		for (int i = 0; i < listDataFromScan.size(); i++) {
			if (i < listDataFromGrid.size()) {
				wasRun = true;
				if(listDataFromGrid.get(i).getBssid().matches(listDataFromScan.get(i).getBssid())) {
					fitness += 0;
				}
				else {
					fitness += 1;
				}
			}
			else {
				break;
			}
		}
		
		if (wasRun == false){
			fitness = Integer.MAX_VALUE;
		}
		return fitness;
	}
	
	
	/* OLD CALCULATE FITNESS FUNCTION
	private static int calculateFitness(ScanResult dataFromGrid,ScanResult dataFromScan){
		int fitness = Integer.MAX_VALUE;
		
		List<ScanResultAp> listDataFromScan = dataFromScan.getListResult(); 
		for (int i = 0; i < listDataFromScan.size(); i++) {
			try {
				ScanResultAp apFromGrid = dataFromGrid.getListResult().get(i);
				ScanResultAp apFromScan = dataFromScan.getListResult().get(i);
				if (apFromScan.getBssid().matches(apFromGrid.getBssid())) {
					fitness = fitness - 1;
				}
				else {
					break;
				}
			}
			catch (Exception e) {
				Static.logExceptionDetail(e);
				break;
			}
		}
		
		return fitness;
	}
	*/

	

	
}


