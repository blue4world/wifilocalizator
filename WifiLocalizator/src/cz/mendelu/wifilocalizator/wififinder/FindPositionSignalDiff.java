package cz.mendelu.wifilocalizator.wififinder;

import java.util.ArrayList;
import java.util.List;

import cz.mendelu.wifilocalizator.dataholers.FindResutlHolder;
import cz.mendelu.wifilocalizator.dataholers.ScanResult;
import cz.mendelu.wifilocalizator.dataholers.ScanResultAp;
import cz.mendelu.wifilocalizator.dataholers.ScanResultsFloors;
import cz.mendelu.wifilocalizator.dataholers.ScanResultsGrid;
import cz.mendelu.wifilocalizator.managers.SettingsManager;

public class FindPositionSignalDiff {
	static final double MAX_DIFF_MULTIPLE = 1.5;
	static final int DIFF_WHEN_NO_AP = 20;
	
	public static FindResutlHolder findPosition(ScanResult dataFromScan,ScanResultsFloors scanResultsFloors) {
		List<FindResutlHolder> findResutlHolders = new ArrayList<FindResutlHolder>();
		
		for (ScanResultsGrid scanResultsGrid : scanResultsFloors.getAllScanResultsGrids()) {
			FindResutlHolder fr = findPositionForFloor(dataFromScan, scanResultsGrid, scanResultsGrid.getFloor());
			findResutlHolders.add(fr);
		}
		
		return FindPosition.getBestFindResultHolder(findResutlHolders);
	}	
	
	private static FindResutlHolder findPositionForFloor(ScanResult dataFromScan,ScanResultsGrid scanResultsGridObj,int floor){
		FindResutlHolder findResutlHolder = new FindResutlHolder();
		findResutlHolder.setFloor(floor);
		
		ScanResult[][] scanResultsGrid = scanResultsGridObj.getScanResultGrid();
		
		int[][] fitnessGrid = FindPosition.createFitnessGrid();
		
		for(int x = 0; x < SettingsManager.GRID_SIZE; x++) {
			for(int y = 0; y < SettingsManager.GRID_SIZE; y++) {
				if (scanResultsGrid[x][y] != null) {
					fitnessGrid[x][y] = calculateFitness(scanResultsGrid[x][y], dataFromScan);
				}
			}
		}
		
		findResutlHolder.addFindResults(fitnessGrid);
		
		
		return findResutlHolder;
	}

	private static int calculateFitness(ScanResult dataFromGrid,ScanResult dataFromScan){
		int fitness = 0;
		int numberOfNotMatch = 0;
		double max_diff = -1;
		Boolean hasRun = false;
		
		
		for (ScanResultAp ap : dataFromScan.getListResult()) {
			int levelFromGrid = dataFromGrid.getLevelOfBssid(ap.getBssid());
			int levelFromScan = ap.getLevel();

			
			if (levelFromGrid != 0) {
				hasRun = true;
				int newDiff = Math.abs(Math.abs(levelFromGrid)-Math.abs(levelFromScan));
				fitness += newDiff;
				/*
				if (newDiff > max_diff) {
					max_diff = newDiff;
				}
				*/
			}
			else {
				numberOfNotMatch++;
			}
		}
		
		//fitness += (numberOfNotMatch*(max_diff*MAX_DIFF_MULTIPLE));
		fitness += (numberOfNotMatch*DIFF_WHEN_NO_AP);
		
		if (!hasRun) {
			fitness = Integer.MAX_VALUE;
		}
		
		/*
		if (max_diff == -1){
			fitness = Integer.MAX_VALUE;
		}
		*/
		
		return fitness;
	}
	
	
	/* OLD CALCULATE FITINESS FUNCTIONS !! IT IS WORKING AND GOOD CODE
	private static int calculateFitness(ScanResult dataFromGrid,ScanResult dataFromScan){
		int fitness = 0;
		
		for (ScanResultAp ap : dataFromScan.getListResult()) {
			int levelFromGrid = dataFromGrid.getLevelOfBssid(ap.getBssid());
			if (levelFromGrid == 0) {
				fitness = 0;
				break;
			}
			int levelFromScan = ap.getLevel();
			
			levelFromGrid = Math.abs(levelFromGrid);
			levelFromScan = Math.abs(levelFromScan);
			
			// TODO here you can add weights for APs!!!
			if (levelFromGrid > levelFromScan) {
				fitness += levelFromGrid - levelFromScan;
			}
			else {
				fitness += levelFromScan - levelFromGrid;
			}
			
		}
		
		if (fitness == 0){
			return Integer.MAX_VALUE;
		}
		else {
			return fitness;
		}
	}
	*/
	
	

	
}


