package cz.mendelu.wifilocalizator.async;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import cz.mendelu.wifilocalizator.R;
import cz.mendelu.wifilocalizator.R.string;
import cz.mendelu.wifilocalizator.dataholers.ScanResultsGrid;
import cz.mendelu.wifilocalizator.managers.ImageSquareManager;
import cz.mendelu.wifilocalizator.other.Constants;
import cz.mendelu.wifilocalizator.other.ManageXmlData;
import cz.mendelu.wifilocalizator.other.MapHelper;
import cz.mendelu.wifilocalizator.other.Static;
import cz.mendelu.wifilocalizator.wifiscan.WifiScanTimer;

public class AsyncProcessScannedData extends AsyncTask<String, String, String>{
	ScanResultsGrid scanResultsGrid;
	ImageSquareManager squareManager;
	WifiScanTimer wifiScan;
	ProgressDialog dialog;
	Context context;
	
	public AsyncProcessScannedData(ScanResultsGrid scanResultsGrid,ImageSquareManager squareManager,WifiScanTimer wifiscan,Context context) {
		this.scanResultsGrid = scanResultsGrid;
		this.squareManager = squareManager;
		this.wifiScan = wifiscan;
		this.context = context;
		this.dialog = new ProgressDialog(context);
	}
	
	@Override
	protected void onPreExecute() {
		dialog.setCancelable(false);
		dialog.setMessage(context.getString(R.string.processing_data));
		dialog.show();
		super.onPreExecute();
	}
	
	@Override
	protected String doInBackground(String... params) {
		try {
			scanResultsGrid.add(squareManager.getGridSquareOrder(), wifiScan.getScannedSquares());
			ManageXmlData.saveFloor(scanResultsGrid,MapHelper.currentFloor);
		}
		catch (Exception e){
			Static.logExceptionDetail(e);
		}
		
		return null;
	}
	
	@Override
	protected void onPostExecute(String result) {
		
		squareManager.cleanUpImageForScan();
		squareManager.reloadDataLayer();
		if (dialog != null && dialog.isShowing()) {
			dialog.dismiss();
		}
		super.onPostExecute(result);
	}

}
