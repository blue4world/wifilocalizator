package cz.mendelu.wifilocalizator.async;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import cz.mendelu.wifilocalizator.R;
import cz.mendelu.wifilocalizator.R.string;
import cz.mendelu.wifilocalizator.dataholers.ScanResultsFloors;
import cz.mendelu.wifilocalizator.dataholers.ScanResultsGrid;
import cz.mendelu.wifilocalizator.managers.ImageSquareManager;
import cz.mendelu.wifilocalizator.neuralnetwork.HopdieldNeuralNetworkHolder;
import cz.mendelu.wifilocalizator.neuralnetwork.NeuralNetworkHolder;
import cz.mendelu.wifilocalizator.other.Constants;
import cz.mendelu.wifilocalizator.other.ManageXmlData;
import cz.mendelu.wifilocalizator.other.MapHelper;
import cz.mendelu.wifilocalizator.other.Static;
import cz.mendelu.wifilocalizator.wifiscan.WifiScanTimer;

public class AsyncTrainNeuralNetwork extends AsyncTask<String, String, String>{
	ProgressDialog dialog;
	Context context;
	NeuralNetworkHolder neuralNetworkHolder;
	ScanResultsFloors scanResultsFloors;
	
	public AsyncTrainNeuralNetwork(ScanResultsFloors scanResultsFloors,NeuralNetworkHolder neuralNetworkHolder,Context context) {
		this.dialog = new ProgressDialog(context);
		this.neuralNetworkHolder = neuralNetworkHolder;
		this.context = context;
		this.scanResultsFloors = scanResultsFloors;
	}
	
	@Override
	protected void onPreExecute() {
		dialog.setCancelable(false);
		dialog.setMessage(context.getString(R.string.learing_neural_netwotk));
		dialog.show();
		super.onPreExecute();
	}
	
	@Override
	protected String doInBackground(String... params) {
		try {
			neuralNetworkHolder.train(scanResultsFloors);
		}
		catch (Exception e){
			Static.logExceptionDetail(e);
		}
		
		return null;
	}
	
	@Override
	protected void onPostExecute(String result) {
		if (dialog != null && dialog.isShowing()) {
			dialog.dismiss();
		}
		super.onPostExecute(result);
	}

}
