package cz.mendelu.wifilocalizator.dataholers;

import java.util.List;

import cz.mendelu.wifilocalizator.managers.SettingsManager;
import cz.mendelu.wifilocalizator.other.Constants;
import cz.mendelu.wifilocalizator.other.Static;

public class TestHolder {
	int x;
	int y;
	double currentError = Double.MAX_VALUE;
	int numberOfTests = 0;
	
	public TestHolder(int x,int y) {
		this.x = x;
		this.y = y;   
	}
	
	public TestHolder(){
		x = Integer.MAX_VALUE;
		y = Integer.MAX_VALUE;
	}
	
	
	public void addResult(List<FindResult> results) throws Exception{
		if (Integer.MAX_VALUE == x || Integer.MAX_VALUE == y){
			throw new Exception(Constants.EXC_testHolderEmpty);
		}
		if(currentError == Double.MAX_VALUE) {
			currentError = 0;
		}
        if (results != null && results.size() > 0) {
			int testX = 0;
	        int testY = 0;
			for(FindResult r : results){
	        	testX = testX + r.getX();
	        	testY = testY + r.getY();
	        }
			
			testX = (int) Math.round((((double)testX) / results.size()));
			testY = (int) Math.round((((double)testY) / results.size()));
			
			double error = Math.pow(Math.abs(testX-x),2) + Math.pow(Math.abs(testY-y),2);
			currentError = currentError + (int)Math.round(Math.sqrt(error));
			numberOfTests++;
        }
	}
	
	public void save(){
		if (numberOfTests > 0 && currentError != Integer.MAX_VALUE) {
		    StringBuilder data = new StringBuilder();
			data.append("-------------------\n");
			data.append("X: "+x+" Y: "+y+"\n");
	        data.append("Time: "+System.currentTimeMillis()+"\n");
	        data.append("Quantity: "+numberOfTests+"\n");
	        data.append("Algorithm: "+SettingsManager.FIND_POSITION_ALGORYTHM+"\n");
	        double error = currentError/numberOfTests;
	        data.append("Error: "+error+"\n");
	        Static.saveDataResultsToFile(data.toString());
	        cleanup();
		}
	}
	
	private void cleanup(){
		x = Integer.MAX_VALUE;
		y = Integer.MAX_VALUE;
		currentError = Double.MAX_VALUE;
		numberOfTests = 0;
	}
	
	public void reset(int x,int y){
		cleanup();
		this.x = x;
		this.y = y;
	}
	
}
