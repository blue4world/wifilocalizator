package cz.mendelu.wifilocalizator.dataholers;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import cz.mendelu.wifilocalizator.managers.SettingsManager;
import cz.mendelu.wifilocalizator.neuralnetwork.HopdieldNeuralNetworkHolder;
import cz.mendelu.wifilocalizator.neuralnetwork.HopfieldNeuralNetwork;
import cz.mendelu.wifilocalizator.neuralnetwork.NeuralNetworkHolder;
import cz.mendelu.wifilocalizator.other.Constants;
import cz.mendelu.wifilocalizator.other.MapHelper;
import cz.mendelu.wifilocalizator.other.Static;
import cz.mendelu.wifilocalizator.wififinder.FindPosition;
import cz.mendelu.wifilocalizator.wififinder.FindPositionApOrder;
import cz.mendelu.wifilocalizator.wififinder.FindPositionEucledianDistance;
import cz.mendelu.wifilocalizator.wififinder.FindPositionEucledianDistanceOptimalized;
import cz.mendelu.wifilocalizator.wififinder.FindPositionSignalDiff;

public class ScanResultsFloors {
	List<ScanResultsGrid> scanResultsGrids = new ArrayList<ScanResultsGrid>();
	NeuralNetworkHolder neuralNetworks;
	Context context;
	
	public ScanResultsFloors(Context context) {
		this.context = context;
		neuralNetworks = new NeuralNetworkHolder(context);
	}
	
	public void addScanResultGrid(ScanResultsGrid scanResultsGrid) {
		scanResultsGrids.add(scanResultsGrid);
	}
	
	public List<ScanResultsGrid> getAllScanResultsGrids(){
		return scanResultsGrids;
	}
	
	public ScanResultsGrid getScanResultsGrid(int floor) {
		ScanResultsGrid foundResultsGrid = null;
		for (ScanResultsGrid scanResultsGrid : scanResultsGrids) {
			if (scanResultsGrid.getFloor() == floor) {
				foundResultsGrid = scanResultsGrid;
			}
		}
		if (foundResultsGrid == null) {
			foundResultsGrid = new ScanResultsGrid(floor);
			scanResultsGrids.add(foundResultsGrid);
			
		}
		return foundResultsGrid;
	}
	
	public NeuralNetworkHolder getNeuralNetworkHolder(){
		return neuralNetworks;
	}
	
	public Boolean isGridSizeOk(int currentGridSize) {
		Boolean result = true;
		for (ScanResultsGrid grid : scanResultsGrids) {
			if (grid.getGridSize() != currentGridSize) {
				result = false;
				break;
			}
		}
		
		return result; 
	}
	
	
	public ScanResultsGrid getCurrentScanResultsGrid(){
		return getScanResultsGrid(MapHelper.currentFloor);
	}
	
	public FindResutlHolder findPosition(ScanResult scanResult) {
		return findPosition(scanResult, null);
	}
	
	
	public FindResutlHolder findPosition(ScanResult scanResult,TestHolder testHolder) {
		FindResutlHolder findResults = null;
		// run only when is data
		if (scanResultsGrids.size() > 0) {
			
			// crop data from scan to accept_ap value from settings
			ScanResult dataFromScan = FindPosition.getDataFromScan(scanResult);
			
			if (SettingsManager.isScanAlgorythmSignalDiff()){
				Static.log("Pouzivam algoritmus signal diff");
				findResults = FindPositionSignalDiff.findPosition(dataFromScan,this);
			}
			else if (SettingsManager.isScanAlgorythmApOrder()){
				Static.log("Pouzivam algoritmus ap order");
				findResults = FindPositionApOrder.findPosition(dataFromScan, this);
			}
			else if (SettingsManager.isScanAlgorythmNeuralNetwork()){
				Static.log("Pouzivam algoritmus neural newtwork");
				findResults = neuralNetworks.getBestResultForData(dataFromScan);
			}
			else if (SettingsManager.isScanAlgorythmEuclideanDistance()){
				Static.log("Pouzivam algoritmus eucledian distance");
				findResults = FindPositionEucledianDistance.findPosition(dataFromScan, this,false);
			}
			else if(SettingsManager.isScanAlgorythmEuclideanDistanceWithShift()) {
				Static.log("Pouzivam algoritmus eucledian distance with Shift");
				findResults = FindPositionEucledianDistanceOptimalized.findPosition(dataFromScan, this,true);
			}
			else {
				Static.log("Neplatny lokalizacni algoritmus");
			}
			
			if(findResults != null && testHolder != null) {
				try {
					testHolder.addResult(findResults.getBestFitness());
				}
				catch (Exception e){
					Static.logExceptionDetail(e);
				}
			}
		}
		else {
			Static.log("Nemam zadna data, nebudu scanovat");
		}
		return findResults;
	}
	
	
}
