package cz.mendelu.wifilocalizator.dataholers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import cz.mendelu.wifilocalizator.managers.SettingsManager;
import cz.mendelu.wifilocalizator.other.Static;

public class ScanResultsGrid {

	//List<List<ScanResult>> scanResultsGrid = new ArrayList<List<ScanResult>>();
	ScanResult[][] scanResultsGrid = new ScanResult [SettingsManager.GRID_SIZE] [SettingsManager.GRID_SIZE];
	Date timestamp = new Date(0);
	int gridSize = 0;
	int floor = 0;
	
	public ScanResultsGrid(int floor){
		this.floor = floor;
	}
	
	
	public int getGridSize(){
		return gridSize;
	}
	
	public Boolean isEmpty(){
		
		Boolean isEmpty = true;
		for(int x = 0; x < SettingsManager.GRID_SIZE; x++) {
			for(int y = 0; y < SettingsManager.GRID_SIZE; y++) {
				if (! isPositionEmpty(x, y) ) {
					isEmpty = false;
					break;
				}
			}
		}
		
		return isEmpty;
	}
	
	
	public void add(GridSquareOrder gridSquareOrder, List<ScanResult> listScanResult) throws Exception {
		int maxOrderNum = gridSquareOrder.getMaxOrderNum();
		for (int i=1;i <= maxOrderNum; i++){
			int[] position = gridSquareOrder.getPositionOfOrderNum(i);
			add(position[0],position[1],listScanResult.get(i-1));
		}
	}
	
	
	public void add(int x,int y,ScanResult newScanResult) throws Exception{
		if (scanResultsGrid[x][y] == null) {
			Static.log("Pridavam nove mereni na pozici: "+x+":"+y);
			scanResultsGrid[x][y] = newScanResult;
		}
		else {
			Static.log("Pridavam mereni a delam prumer na pozici: "+x+":"+y);
			scanResultsGrid[x][y].addWithAverage(newScanResult);
		}
	}
	
	
	public Boolean isPositionEmpty(int x,int y){
		if (scanResultsGrid[x][y] == null) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public ScanResult getScanResultFromCoordinates(int[] coordinates){
		return scanResultsGrid[coordinates[0]][coordinates[1]];
	}
	
	public void deleteScanResultFromCoordinates(int[] coordinates){
		if( ! isPositionEmpty(coordinates[0],coordinates[1]) ) {
			scanResultsGrid[coordinates[0]][coordinates[1]] = null;
		}
	}
	
	public void printAll(){
		for(int x = 0; x < SettingsManager.GRID_SIZE; x++) {
			for(int y = 0; y < SettingsManager.GRID_SIZE; y++) {
				if (! isPositionEmpty(x, y) ) {
					Static.log("X: "+x+" Y: "+y);
					Static.log(scanResultsGrid[x][y].getInfo());
					Static.log("--------");
				}
			}
		}
	}
	
	public int getNumberOfScanResultAp(){
		int number = 0;
		for(int x = 0; x < SettingsManager.GRID_SIZE; x++) {
			for(int y = 0; y < SettingsManager.GRID_SIZE; y++) {
				if (! isPositionEmpty(x, y) ) {
					number++;
				}
			}
		}
		return number;
	}
	
	public void setTimestamp(String timestamp) {
		try {
			this.timestamp = new Date(Long.parseLong(timestamp));
		}
		catch (Exception e){
			Static.logExceptionDetail(e);
		}
	}
	
	public void setGridSize(int gridSize){
		this.gridSize = gridSize;
	}
	
	public List<String> getAllBssidSortedByName(){
		Set<String> bssid = new HashSet<String>();
		
		for(int x = 0; x < SettingsManager.GRID_SIZE; x++) {
			for(int y = 0; y < SettingsManager.GRID_SIZE; y++) {
				if (! isPositionEmpty(x, y) ) {
					for (ScanResultAp scanResultAp : scanResultsGrid[x][y].getListResult() ) {
						bssid.add(scanResultAp.getBssid());
					}
				}
			}
		}
		
		List<String> bssidString = new ArrayList<String>(bssid);
		// THIS IS FOR SORTING
		java.util.Collections.sort(bssidString);
		
		return bssidString;
	}
	
	public List<String> getAllBssidSortedByCount(){
		HashMap<String,Integer> hashOfBssid = new HashMap<String, Integer>();
		for(int x = 0; x < SettingsManager.GRID_SIZE; x++) {
			for(int y = 0; y < SettingsManager.GRID_SIZE; y++) {
				if (! isPositionEmpty(x, y) ) {
					for (ScanResultAp scanResultAp : scanResultsGrid[x][y].getListResult() ){
						Integer num = hashOfBssid.get(scanResultAp.bssid);
						if (num == null) {
							hashOfBssid.put(scanResultAp.bssid, 1);
						}
						else {
							hashOfBssid.put(scanResultAp.bssid, num+1);
						}
					}
				}
			}
		}
		
		// sort map
		Map<String, Integer> sortedHashOfBssid = Static.sortByComparator(hashOfBssid, false);
		// from map to array
		List<String> listOfBssid = new ArrayList<String>();
		for (Map.Entry<String, Integer> entry : sortedHashOfBssid.entrySet()) {
	        //System.out.println("Key : " + entry.getKey() + " Value : "+ entry.getValue());
			listOfBssid.add(entry.getKey());
	    }
		
		return listOfBssid;
	}
		
	
	public List<String> getAllBssid(){
		Set<String> listOfBssid = new HashSet<String>();
		for(int x = 0; x < SettingsManager.GRID_SIZE; x++) {
			for(int y = 0; y < SettingsManager.GRID_SIZE; y++) {
				if (! isPositionEmpty(x, y) ) {
					for (ScanResultAp scanResultAp : scanResultsGrid[x][y].getListResult() ){
						listOfBssid.add(scanResultAp.bssid);
					}
				}
			}
		}
		
		List<String> listOfBssidString = new ArrayList<String>(listOfBssid);
		
		return listOfBssidString;
	}
	
		
	
	public ScanResult[][] getScanResultGrid(){
		return scanResultsGrid;
	}
	
	public int getFloor(){
		return floor;
	}
}
