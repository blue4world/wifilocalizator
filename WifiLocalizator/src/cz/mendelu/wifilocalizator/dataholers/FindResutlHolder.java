package cz.mendelu.wifilocalizator.dataholers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.os.Bundle;

import cz.mendelu.wifilocalizator.managers.SettingsManager;
import cz.mendelu.wifilocalizator.other.Constants;

public class FindResutlHolder {
	List<FindResult> findResults = new ArrayList<FindResult>();
	int floor = 0;
	
	public FindResutlHolder() { }
	
	public FindResutlHolder(Bundle data) {
		int numOfResults = data.getInt(Constants.MSG_numOfPosition);
		for (int i = 0;i < numOfResults; i++) {
    		int x = data.getInt(Constants.MSG_x+i);
    		int y = data.getInt(Constants.MSG_y+i);
    		int fitness = data.getInt(Constants.MSG_fitness+i);
    		findResults.add(new FindResult(x, y, fitness));
		}
		this.floor = data.getInt(Constants.MSG_floor);
		sortResults();
	}
	
	public void addFindResult(int fitness,int x,int y){
		findResults.add(new FindResult(x, y, fitness));
		sortResults();
	}
	
	public void addFindResults(int[][] fitnessGrid){
		for(int x = 0; x < SettingsManager.GRID_SIZE; x++) {
			for(int y = 0; y < SettingsManager.GRID_SIZE; y++) {
				if (fitnessGrid[x][y] != Integer.MAX_VALUE) {
					findResults.add(new FindResult(x,y,fitnessGrid[x][y]));
				}
			}
		}
		sortResults();
	}
	
	private void sortResults() {
		Collections.sort(findResults,new FindResultComparator());
	}

	
	
	public Bundle getBundle() {
		Bundle data = new Bundle();
		int i = 0;
		for (FindResult fr : findResults) {
	    	data.putInt(Constants.MSG_x+i, fr.getX());
	    	data.putInt(Constants.MSG_y+i, fr.getY());
	    	data.putInt(Constants.MSG_fitness+i, fr.getFitness());
	    	i++;
		}
		data.putInt(Constants.MSG_numOfPosition, i);
		data.putInt(Constants.MSG_floor, floor);
		return data;
	}
	
	
	public int getBestFitnessValue(){
		if (findResults.size() > 0) {
			return findResults.get(0).getFitness();
		}
		else {
			return Integer.MAX_VALUE;
		}
	}
	
	
	
	public List<FindResult> getBestFitness(){
		
		Set<FindResult> bestFitness = new HashSet<FindResult>();
		int resultToShow = SettingsManager.FIND_POSITION_NUM_OF_RESULT_TO_SHOW;
		
		if (resultToShow > findResults.size()) {
			resultToShow = findResults.size();
		}
		
		Boolean found = true;
		for(int i=0;i<resultToShow;i++) {
			bestFitness.add(findResults.get(i));
			int j = i;
			do {
				if ( (j+1) < findResults.size()) {
					if (findResults.get(j).getFitness() == findResults.get(j+1).getFitness()) {
						bestFitness.add(findResults.get(j+1));
						found = true;
					}
					else {
						found = false;
					}
				}
				else {
					found = false;
				}
				j++;
			} while(found);
		}
		
		return new ArrayList<FindResult>(bestFitness);
		
		/*
		List<FindResult> bestFitness = new ArrayList<FindResult>();
		if (findResults.size() > 0) {
			int lastFitness = findResults.get(0).getFitness();  
			for (FindResult fr : findResults) {
				if (lastFitness == fr.getFitness()) {
					bestFitness.add(fr);
				}
				else {
					break;
				}
				lastFitness = fr.getFitness();
			}
				
		}
		return bestFitness;
		*/
	}
	
	public static int[] getPositionOFBestFintessValue(List<FindResult> findResults){
		int[] cooridnates = new int[2];
		int bestFintess = Integer.MAX_VALUE;
		for(int i=0;i<findResults.size();i++){
			if (findResults.get(i).getFitness() < bestFintess) {
				bestFintess = findResults.get(i).getFitness();
				cooridnates = findResults.get(i).getPosition();
			}
		}
		
		return cooridnates;
	}
	
	public Boolean isEmpty(){
		if (findResults.size() == 0){
			return true;
		}
		else {
			return false;
		}
	}
	
	
	public List<FindResult> getAll(){
		return findResults;
	}
	
	public int getFloor(){
		return floor;
	}
	
	public void setFloor(int floor){
		this.floor = floor;
	}

	
	public class FindResultComparator implements Comparator<FindResult> {
		
		@Override
		public int compare(FindResult arg0, FindResult arg1) {
			
			if (arg0.getFitness() > arg1.getFitness()) {
				return 1;
			}
			else if (arg0.getFitness() < arg1.getFitness()){
				return -1;
			}
			else {
				return 0;
			}
			
		}
	}
}
