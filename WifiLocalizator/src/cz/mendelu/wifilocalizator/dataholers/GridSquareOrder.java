package cz.mendelu.wifilocalizator.dataholers;

import cz.mendelu.wifilocalizator.managers.SettingsManager;

public class GridSquareOrder {
	int [][] gridSquareOrder = new int[SettingsManager.GRID_SIZE][SettingsManager.GRID_SIZE];
	int maxOrderNum = 0;
	
	public void add(int x,int y,int orderNum){
		gridSquareOrder[x][y] = orderNum;
		if (orderNum > maxOrderNum) {
			maxOrderNum = orderNum;
		}
	}
	
	public void clean(){
		for(int x=0;x<SettingsManager.GRID_SIZE;x++) {
			for(int y=0;y<SettingsManager.GRID_SIZE;y++) {
				gridSquareOrder[x][y] = 0;
			}
		}
		maxOrderNum = 0;
	}
	
	public int getMaxOrderNum(){
		return maxOrderNum;
	}
	
	public int getOrderNumOfPosition(int[] position){
		return gridSquareOrder[position[0]][position[1]];
	}
	
	public void deleteLastSquare() {
		int[] position = getPositionOfOrderNum(maxOrderNum);
		gridSquareOrder[position[0]][position[1]] = 0;
		maxOrderNum = maxOrderNum -1;
	}
	
	public int[] getPositionOfOrderNum(int orderNum){
		int[] position = new int[2];
		for(int x=0;x<SettingsManager.GRID_SIZE;x++) {
			for(int y=0;y<SettingsManager.GRID_SIZE;y++) {
				if (gridSquareOrder[x][y] == orderNum) {
					position[0] = x;
					position[1] = y;
					break;
				}
			}
		}
		return position;
	}
	
		
}
