package cz.mendelu.wifilocalizator.dataholers;

public class FindResult {
	int[] position = new int[2];
	int fitness = Integer.MAX_VALUE;
	
	
	public FindResult(int x,int y,int fitness) {
		position[0] = x;
		position[1] = y;
		this.fitness = fitness;
	}
	
	public FindResult() {
		position[0] = Integer.MAX_VALUE;
		position[1] = Integer.MAX_VALUE;
	}
	
	public void setPosition(int x,int y){
		position[0] = x;
		position[1] = y;
	}
	
	public int[] getPosition(){
		return position;
	}
	
	public void setFitness(int fitness){
		this.fitness = fitness;
	}
	
	public int getFitness(){
		return fitness;
	}
	
	public String getInfo(){
		return "X: "+position[0]+" Y: "+position[1]+" Fitness: "+fitness;
	}
	
	public int getX(){
		return position[0];
	}
	
	public int getY(){
		return position[1];
	}
}
