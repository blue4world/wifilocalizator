package cz.mendelu.wifilocalizator.dataholers;

import cz.mendelu.wifilocalizator.other.Constants;

public class DataLayerType {
	String dataLayerType;
	
	public DataLayerType() {
		dataLayerType = Constants.DATA_LAYER_NONE;
	}
	
	public DataLayerType(String dataLayerType){
		this.dataLayerType = dataLayerType;
	}
	
	public void setDataLayerType(String dataLayerType) {
		this.dataLayerType = dataLayerType;
	}
	
	public String getDataLayerType() {
		return dataLayerType;
	}
	
	
}
