package cz.mendelu.wifilocalizator.dataholers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import cz.mendelu.wifilocalizator.managers.SettingsManager;
import cz.mendelu.wifilocalizator.other.Constants;
import cz.mendelu.wifilocalizator.other.Static;


public class ScanResult {
	List<ScanResultAp> results = new ArrayList<ScanResultAp>();
	
	
	private void addWithAverage(ScanResultAp scanResultAp) throws Exception {
		/*
		// dont add faro-free
		if (Constants.SCANNER_DONT_SCAN_FARO_FREE) {
			if (scanResultAp.getSsid().matches(Constants.FARO_FREE_SSID)) {
				return;
			}
		}
		*/
		
		if(SettingsManager.EDUROAM_ONLY == 1){
			if(!scanResultAp.getSsid().matches(Constants.ERUDOAM_SSID)){
				return;
			}
		}
		
		
		Boolean found = false;
		for (ScanResultAp r : results) {
			if ( r.getBssid().matches(scanResultAp.getBssid()) ) {
				found = true;
				//int newLevel = ( r.getLevel() + scanResultAp.getLevel() ) / 2;
				r.addLevel(scanResultAp.getLevel());
				break;
			}
		}
		
		if (! found ) {
			results.add(scanResultAp);
		}
		sortResultsByLevel();
	}
	
	public void addWithAverage(ScanResult scanResult) throws Exception{
		for (ScanResultAp r : scanResult.getListResult()) {
			addWithAverage(r);
		}
	}
	
	public void addWithAverage(ScanResult scanResult,int numOfItemToAdd) throws Exception{
		int i = 0;
		for (ScanResultAp r : scanResult.getListResult()) {
			if (i == numOfItemToAdd) {
				break;
			}
			addWithAverage(r);
			i++;
		}
	}
	
	public Boolean containBssid(String bssid){
		Boolean contain = false;
		for (ScanResultAp ap : results) {
			if (ap.getBssid().matches(bssid)) {
				contain = true;
				break;
			}
		}
		return contain;
	}
	
	public int getLevelOfBssid(String bssid){
		ScanResultAp ap = getScanResultAp(bssid);
		if (ap != null) {
			return ap.getLevel();
		}
		else {
			return 0;
		}
	}
	
	public int getAverageOfStandartDeviation(){
		int sum = 0;
		int i = 0;
		for(ScanResultAp ap : results){
			sum += ap.getStandartDeviation();
			i++;
		}
		return (int)Math.round((sum/(double)i));
	}
	
	
	public ScanResultAp getScanResultAp(String bssid){
		ScanResultAp result = null;
		for (ScanResultAp ap : results) {
			if (ap.getBssid().matches(bssid)) {
				result = ap;
				break;
			}
		}
		return result;
	}
	
	public void addForceNoSort(ScanResultAp scanResultAp) {
		// only eduroam
		if(SettingsManager.EDUROAM_ONLY == 1){
			if(!scanResultAp.getSsid().matches(Constants.ERUDOAM_SSID)){
				return;
			}
		}
		results.add(scanResultAp);
	}
	
	private void sortResultsByLevel(){
		Collections.sort(results,new ScanResultApComparator());
	}
	
	
	
	public int getScanResultsCount(){
		return results.size();
	}
	
	public List<ScanResultAp> getListResult(){
		return results;
	}
	
	/*
	public void print() {
		for (ScanResultAp ap: results) {
			Static.log("SSID: "+ap.getSsid()+" BSSID: "+ap.getBssid()+" Level: "+ap.getLevel());
		}
	}
	*/
	
	public String getInfo() {
		String info = "";
		for (ScanResultAp ap: results) {
			info+= ap.getBssid()+":"+ap.getLevel()+" ("+ap.getStandartDeviation()+")\n";
		}
		return info;
	}
	
	
	
	public class ScanResultApComparator implements Comparator<ScanResultAp> {
		@Override
		public int compare(ScanResultAp arg0, ScanResultAp arg1) {
			
			if (arg0.getLevel() > arg1.getLevel()) {
				return -1;
			}
			else if (arg0.getLevel() < arg1.getLevel()){
				return 1;
			}
			else {
				return 0;
			}
			
		}
	}

}
