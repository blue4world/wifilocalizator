package cz.mendelu.wifilocalizator.dataholers;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

public class ScanResultAp {
	String bssid = "";
	String ssid = "";
	Set<Integer> allLevels = new HashSet<Integer>();
	int level = 0;
	
	public ScanResultAp(){ }
	
	public ScanResultAp(String ssid,String bssid,int level) {
		this.ssid = ssid;
		this.bssid = bssid;
		this.level = level;
	}
	
	public void setAllLevels(String input){
		String[] parts = Pattern.compile(",", Pattern.LITERAL).split(input);
		for (int i=0;i<parts.length;i++){
			if (!parts[i].matches("")) {
				allLevels.add(Integer.valueOf(parts[i]));
			}
		}
	}
	
	public int getStandartDeviation(){
		int sum = 0;
		for(Integer i : allLevels) {
			sum += Math.pow(i-level,2);
		}
		int result = (int)Math.round((double)sum / (double)(allLevels.size()-1));
		result = (int)Math.round(Math.sqrt(result));
		return result;
	}
	
	public String getAllLevels(){
		String result = "";
		for(Integer s : allLevels){
			result += s+",";
		}
		return result;
	}
	
	public Set<Integer> getAllLevelsArray(){
		return allLevels;
	}
	
	public void setBssid(String bssid){
		this.bssid = bssid;
	}
	
	public void setSsid(String ssid){
		this.ssid = ssid;
	}
	
	
	public void setLevel(int level){
		this.level = level;
	}
	
	public void addLevel(int newLevel){
		level = ( level + newLevel ) / 2;
		allLevels.add(newLevel);
	}
	
	public String getBssid(){
		return bssid;
	}
	
	public int getLevel(){
		return level;
	}
	
	public String getSsid(){
		return ssid;
	}
}
