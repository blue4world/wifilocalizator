package cz.mendelu.wifilocalizator.neuralnetwork;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import junit.framework.Test;

import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataPair;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;

import cz.mendelu.wifilocalizator.dataholers.FindResutlHolder;
import cz.mendelu.wifilocalizator.dataholers.ScanResult;
import cz.mendelu.wifilocalizator.dataholers.ScanResultAp;
import cz.mendelu.wifilocalizator.dataholers.ScanResultsGrid;
import cz.mendelu.wifilocalizator.managers.SettingsManager;
import cz.mendelu.wifilocalizator.other.Constants;
import cz.mendelu.wifilocalizator.other.Static;
import cz.mendelu.wifilocalizator.wififinder.FindPosition;

public class NeuralNetwork {
	final double NO_AP_NEURAL_INIT_VALUE = 0.0;
	final double NEURAL_ERROR = 0.01;
	BasicNetwork network;
	List<String> allBssid;
	int floor;
	int numOfTestData;
	
	public NeuralNetwork(ScanResultsGrid scanResultsGrid,int floor) {
		this.floor = floor;
		if (scanResultsGrid.getNumberOfScanResultAp() > 0) {
			train(scanResultsGrid);
		}
		else {
			Static.log("Nebudu trenovat neuronovou sit protoze nemam data pro podlazi: "+floor);
		}
	}
	
	public void train(ScanResultsGrid scanResultsGrid) {
		allBssid = scanResultsGrid.getAllBssid();
		numOfTestData = scanResultsGrid.getNumberOfScanResultAp();
		
		List<NeuralData> neuralDataList = new ArrayList<NeuralData>();
		
		// prepare data
		ScanResult[][] scanResults = scanResultsGrid.getScanResultGrid();
		for (int x = 0; x < SettingsManager.GRID_SIZE; x++) {
			for (int y = 0; y < SettingsManager.GRID_SIZE; y++) {
				if (scanResults[x][y] != null) {
					for (ScanResultAp ap : scanResults[x][y].getListResult()){
						for(Integer i : ap.getAllLevelsArray()) {
							ScanResult newScanResult = new ScanResult();
							ScanResultAp newAp = new ScanResultAp();
							newAp.setBssid(ap.getBssid());
							newAp.setSsid(ap.getSsid());
							newAp.setLevel(i);
							newScanResult.addForceNoSort(newAp);
							for (ScanResultAp ap2 : scanResults[x][y].getListResult()) {
								newScanResult.addForceNoSort(ap2);
							}
							neuralDataList.add(new NeuralData(convertScanResulForNeural(newScanResult), new double[]{x,y}));
						}
					}
				}
			}
		}
		
		Static.log("Velikost ucicich dat je: "+neuralDataList.size());
		
		double[][] INPUT = convertListOfNeuralDataToInput(neuralDataList);
		double[][] OUTPUT = convertListOfNeuralDataToOutput(neuralDataList);
				
		// create a neural network
		network = new BasicNetwork();
		network.addLayer(new BasicLayer(null, true, allBssid.size()));
		for (int i=0;i<allBssid.size();i++) {
			network.addLayer(new BasicLayer(new ActivationSigmoid(), true, allBssid.size()*2));
		}
		network.addLayer(new BasicLayer(new ActivationSigmoid(), false,2));
		network.getStructure().finalizeStructure();
		network.reset();

		
		// create training data
		MLDataSet trainingSet = new BasicMLDataSet(INPUT, OUTPUT);
		
		// train the neural network
		final ResilientPropagation train = new ResilientPropagation(network,trainingSet);
		int epoch = 1;
		Static.log("Zacinam ucit neuronovou sit pro patro: "+floor);
		do {
			train.iteration();
			Static.log("Epoch #" + epoch + " Error:" + train.getError());
			epoch++;
			if (epoch > 2000){
				Static.log("Uceni prekrocilo max pocet epoch");
				break;
			}
		} while (train.getError() > NEURAL_ERROR);
		train.finishTraining();
		
		// Encog.getInstance().shutdown();
		
	}
	
	private double[][] convertListOfNeuralDataToInput(List<NeuralData> dataList){
		double[][] result = new double[dataList.size()][allBssid.size()];
		int i=0;
		for(NeuralData data : dataList){
			result[i] = data.getInput(); 
			i++;
		}
		return result;
	}
	
	private double[][] convertListOfNeuralDataToOutput(List<NeuralData> dataList){
		double[][] result = new double[dataList.size()][2];
		int i=0;
		for(NeuralData data : dataList){
			result[i] = data.getOutput(); 
			result[i][0] = result[i][0] / 1000.0;
			result[i][1] = result[i][1] / 1000.0;
			i++;
		}
		return result;
	}
	
	// find X,Y
	public FindResutlHolder find(ScanResult dataFromScan){
		
		FindResutlHolder results = new FindResutlHolder();
		results.setFloor(floor);
		
		double[] scannedData = convertScanResulForNeural(dataFromScan);
		
		// compute for scanned data
		double[] networkResult = new double[2];

		network.compute(scannedData, networkResult);
		
		int x = (int) (networkResult[0]*1000.0);
		int y = (int) (networkResult[1]*1000.0);
		if (x < SettingsManager.GRID_SIZE && y < SettingsManager.GRID_SIZE){
			results.addFindResult(1,x ,y );
		}
		else {
			Static.log("Vysledek x,y je vetsi nez gridSize");
			Static.log("x: "+x+"y: "+y);
		}
		
		
		return results;
		
	}
	
			
	public double[] convertScanResulForNeural(ScanResult scanResults) {
		Integer[] result = new Integer[allBssid.size()];

		// init
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		for (int i = 0; i < allBssid.size(); i++) {
			map.put(allBssid.get(i), (int)NO_AP_NEURAL_INIT_VALUE);
		}

		// add values from scanResult
		for (ScanResultAp ap : scanResults.getListResult()) {
			if (map.containsKey(ap.getBssid())) {
				int level = Math.abs(ap.getLevel());
				if (level > 100) {
					level = 100;
				}
				level = 100 - level;
				map.put(ap.getBssid(), level);
			}
		}

		// put sorted values to int array
		int i = 0;
		SortedSet<String> keys = new TreeSet<String>(map.keySet());
		for (String key : keys) {
			result[i] = map.get(key);
			i++;
		}

		// convert Integer[] to double[]
		double[] resultDouble = new double[allBssid.size()];
		for (i = 0; i < allBssid.size(); i++) {
			resultDouble[i] = (double) ((int) result[i]);
		}

		return resultDouble;
	}

	

}
