package cz.mendelu.wifilocalizator.neuralnetwork;

import java.util.HashMap;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import org.encog.ml.data.specific.BiPolarNeuralData;
import org.encog.neural.thermal.HopfieldNetwork;

import cz.mendelu.wifilocalizator.dataholers.FindResutlHolder;
import cz.mendelu.wifilocalizator.dataholers.ScanResult;
import cz.mendelu.wifilocalizator.dataholers.ScanResultAp;
import cz.mendelu.wifilocalizator.dataholers.ScanResultsGrid;
import cz.mendelu.wifilocalizator.managers.SettingsManager;
import cz.mendelu.wifilocalizator.other.Static;

/**
 * Simple class to recognize some patterns with a Hopfield Neural Network.
 * This is very loosely based on a an example by Karsten Kutza, 
 * written in C on 1996-01-30.
 * http://www.neural-networks-at-your-fingertips.com/hopfield.html
 * 
 * I translated it to Java and adapted it to use Encog for neural
 * network processing.  I mainly kept the patterns from the 
 * original example.
 *
 */
public class HopfieldNeuralNetwork {
	List<String> allBssid;
	final double NO_AP_NEURAL_INIT_VALUE = 0.0;
	final int SIZE_OF_ROW = 30;
	HopfieldNetwork hopfieldLogic;
	int floor;
	Boolean runOne = true;
	BiPolarNeuralData[][] bipolarGrid = new BiPolarNeuralData[SettingsManager.GRID_SIZE][SettingsManager.GRID_SIZE];
	
	public HopfieldNeuralNetwork(ScanResultsGrid scanResultsGrid,int floor) {
		this.floor = floor;
		if (scanResultsGrid.getNumberOfScanResultAp() > 0) {
			train(scanResultsGrid);
		}
		else {
			Static.log("Nebudu trenovat neuronovou sit protoze nemam data pro podlazi: "+floor);
		}
	}
		
	public void train(ScanResultsGrid scanResultsGrid) {	
		allBssid = scanResultsGrid.getAllBssid();
		
		hopfieldLogic = new HopfieldNetwork(allBssid.size()*SIZE_OF_ROW);
		
		Static.deleteDataForNeuralFromFile();
		ScanResult[][] scanResultGrid = scanResultsGrid.getScanResultGrid();
		for(int x=0;x<SettingsManager.GRID_SIZE;x++){
			for(int y=0;y<SettingsManager.GRID_SIZE;y++){
				if (scanResultGrid[x][y] != null) {
						BiPolarNeuralData dataForCell = convertScanResulForNeural(scanResultGrid[x][y]);
						hopfieldLogic.addPattern(dataForCell);
						bipolarGrid[x][y] = dataForCell;
						Static.saveDataForNeuralToFile("\nLEARN:\n");
						Static.saveDataForNeuralToFile(showData(dataForCell));
				}
			}
		}

	}
	
	public FindResutlHolder find(ScanResult dataFromScan){
			FindResutlHolder findResutlHolder = new FindResutlHolder();
			findResutlHolder.setFloor(floor);
		
			BiPolarNeuralData input = convertScanResulForNeural(dataFromScan);
			hopfieldLogic.setCurrentState(input);
			int cycles = hopfieldLogic.runUntilStable(Integer.MAX_VALUE);
			BiPolarNeuralData output = (BiPolarNeuralData)hopfieldLogic.getCurrentState();
			System.out.println("Cycles until stable(max 100): " + cycles + ", result=");
			
			
			Static.saveDataForNeuralToFile("\n\n\n\nINPUT: \n");
			Static.saveDataForNeuralToFile(showData(input));
			Static.saveDataForNeuralToFile("\n\nOUTPUT\n");
			Static.saveDataForNeuralToFile(showData(output));
			
			
			int[] coordinates = getBestXYforResult(output);
			if (coordinates[0] == Integer.MAX_VALUE && coordinates[1] == Integer.MAX_VALUE) {
				Static.log("Neuronoval sit nedala zadny vysledek");
			}
			else {
				findResutlHolder.addFindResult(1, coordinates[0], coordinates[1]);
				
			}
		
		return findResutlHolder;
	}
	
	
	private int[] getBestXYforResult(BiPolarNeuralData computedDataBipolar){
		int[] coordinates = new int[2];
		coordinates[0] = Integer.MAX_VALUE;
		coordinates[1] = Integer.MAX_VALUE;
		String computedData = showData(computedDataBipolar);
		
		for(int x=0;x<SettingsManager.GRID_SIZE;x++){
			for(int y=0;y<SettingsManager.GRID_SIZE;y++){
				if (bipolarGrid[x][y] != null) {
					if(computedData.matches(showData(bipolarGrid[x][y]))) {
						coordinates[0] = x;
						coordinates[1] = y;
						return coordinates;
					}
				}
			}
		}
		
		return coordinates;
	}
		
	private String showData(BiPolarNeuralData data){
		String string = "";
		for(int i=0;i<allBssid.size();i++){
			for(int j=0;j<SIZE_OF_ROW;j++){
				if (data.getBoolean((i*SIZE_OF_ROW)+j)) {
					string += "#";
				}
				else {
					string += "O";
				}
			}
			string += "\n";
		}
		return string;
	}
	
	
	public BiPolarNeuralData convertScanResulForNeural(ScanResult scanResults) {
		Integer[] result = new Integer[allBssid.size()];

		// init
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		for (int i = 0; i < allBssid.size(); i++) {
			map.put(allBssid.get(i), (int)NO_AP_NEURAL_INIT_VALUE);
		}

		// add values from scanResult
		for (ScanResultAp ap : scanResults.getListResult()) {
			if (map.containsKey(ap.getBssid())) {
				int level = Math.abs( (int) Math.round(ap.getLevel()/(double)SIZE_OF_ROW) );
				if (level > SIZE_OF_ROW) {
					level = SIZE_OF_ROW;
				}
				level = SIZE_OF_ROW - level;
				map.put(ap.getBssid(), level);
			}
		}

		// put sorted values to int array
		int i = 0;
		SortedSet<String> keys = new TreeSet<String>(map.keySet());
		for (String key : keys) {
			result[i] = map.get(key);
			i++;
		}
		

		BiPolarNeuralData neuralResult = new BiPolarNeuralData(allBssid.size()*SIZE_OF_ROW);
		for(int j=0;j<result.length;j++){
			Integer value = result[j];
			int valueCount = 0;
				for (int k=0;k<SIZE_OF_ROW;k++){
					if (value < valueCount) {
						neuralResult.setData(((j*SIZE_OF_ROW)+k), true);
					}
					else {
						neuralResult.setData(((j*SIZE_OF_ROW)+k), false);
					}
					valueCount++;
				}
		}
	
		return neuralResult;
	}
	
	/*
	public static void main(String[] args)
	{
		HopfieldNeuralNetwork program = new HopfieldNeuralNetwork();
		program.run();
		Encog.getInstance().shutdown();
	}
	*/
	
}
