package cz.mendelu.wifilocalizator.neuralnetwork;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;
import org.encog.persist.EncogDirectoryPersistence;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import cz.mendelu.wifilocalizator.dataholers.FindResutlHolder;
import cz.mendelu.wifilocalizator.dataholers.ScanResult;
import cz.mendelu.wifilocalizator.dataholers.ScanResultAp;
import cz.mendelu.wifilocalizator.dataholers.ScanResultsGrid;
import cz.mendelu.wifilocalizator.managers.SettingsManager;
import cz.mendelu.wifilocalizator.other.Constants;
import cz.mendelu.wifilocalizator.other.Static;
import cz.mendelu.wifilocalizator.wififinder.FindPosition;

public class NeuralNetworkGripPowerOutput {
	final double NO_AP_NEURAL_INIT_VALUE = 0.0;
	final double NEURAL_ERROR = 0.001;
	//final int NUM_OF_FITNESSGRID_TO_COMPUTE = 1;
	final int MAX_EPOCH = 100;
	double multiplyConstant = 100;
	BasicNetwork network;
	List<String> allBssid;	
	//List<int[][]> fitnessGridHolder = new ArrayList<int[][]>();
	ScanResultsGrid scanResultsGrid;
	NeuralDataHolder neuralDataHolder;
	int floor;
	Context context;
	MLDataSet trainingSet;
	
	public NeuralNetworkGripPowerOutput(ScanResultsGrid scanResultsGrid,int floor,Context context) {
		this.floor = floor;
		this.context = context;
		this.scanResultsGrid = scanResultsGrid;
		this.allBssid = scanResultsGrid.getAllBssid();
		if (getMd5OfFile(getFileDataGrid(floor)).matches(getSavedMd5OfForFloor(floor)) && getFileForNeural(floor).exists()) {
			Static.log("Nacitam neuronovou sit ze souboru");
			network = (BasicNetwork)EncogDirectoryPersistence.loadObject(getFileForNeural(floor));
		}
		else {
			if (scanResultsGrid.getNumberOfScanResultAp() > 0) {
				Static.log("Ucim neuronovou sit");
				train(scanResultsGrid);
			}
			else {
				Static.log("Nebudu trenovat neuronovou sit protoze nemam data pro podlazi: "+floor);
			}
		}
	}
	
	public void train(ScanResultsGrid scanResultsGrid) {
		
		neuralDataHolder = new NeuralDataHolder();
		neuralDataHolder.add(new NeuralData(getEmptyInput(), getEmptyOutput()));		
		// prepare data
		ScanResult[][] scanResults = scanResultsGrid.getScanResultGrid();
		for (int x = 0; x < SettingsManager.GRID_SIZE; x++) {
			for (int y = 0; y < SettingsManager.GRID_SIZE; y++) {
				if (scanResults[x][y] != null) {
					ScanResult scanResult = new ScanResult();
					try {
						scanResult.addWithAverage(scanResults[x][y], SettingsManager.FIND_POSITION_NUMBER_OF_ACCEPTED_AP);
					}
					catch (Exception e){
						Static.logExceptionDetail(e);
						scanResult = scanResults[x][y];
					}
					
					for (ScanResultAp ap : scanResult.getListResult()){
						for(Integer i : ap.getAllLevelsArray()) {
							ScanResult newScanResult = new ScanResult();
							ScanResultAp newAp = new ScanResultAp();
							newAp.setBssid(ap.getBssid());
							newAp.setSsid(ap.getSsid());
							newAp.setLevel(i);
							newScanResult.addForceNoSort(newAp);
							
							for (ScanResultAp ap2 : scanResult.getListResult()) {
								if (! ap2.getBssid().matches(newAp.getBssid())) {
									newScanResult.addForceNoSort(ap2);
								}
							}
							neuralDataHolder.add(new NeuralData(convertScanResulForNeural(newScanResult), new double[]{x,y}));
						}
					}
				}
			}
		}
		
		Static.log("Velikost ucicich dat je: "+neuralDataHolder.getSize());
		Static.saveDataForNeuralToFile("Velikost ucicich dat je: "+neuralDataHolder.getSize());
		
		neuralDataHolder.removeDuplicateData();
		
		Static.log("Velikost ucicich po odstreneni duplicit je: "+neuralDataHolder.getSize());
		Static.saveDataForNeuralToFile("Velikost ucicich po odstreneni duplicit je: "+neuralDataHolder.getSize());
		
		
		double[][] INPUT = convertListOfNeuralDataToInput(neuralDataHolder.getNeuralData());
		double[][] OUTPUT = convertListOfNeuralDataToOutput(neuralDataHolder.getNeuralData());
		
		/*
		// create a neural network
		network = new BasicNetwork();
		network.addLayer(new BasicLayer(null, true, allBssid.size()));
		for(int i=0;i>allBssid.size();i++){
			network.addLayer(new BasicLayer(new ActivationSigmoid(), true, allBssid.size()*3));
		}
		network.addLayer(new BasicLayer(new ActivationSigmoid(), false,Constants.NUMBER_OF_CELLS_IN_GRID_POWER));
		network.getStructure().finalizeStructure();
		network.reset();
		*/
		
		network = new BasicNetwork();
		network.addLayer(new BasicLayer(null, true, allBssid.size()));
		int numberOfHiddenLayer = (int) Math.round(Math.sqrt(allBssid.size()*Constants.NUMBER_OF_CELLS_IN_GRID_POWER));
		int numberOfNeuronsInHiddenLayer = (int) Math.round(Constants.NUMBER_OF_CELLS_IN_GRID_POWER*0.8);
		for(int i=0;i>numberOfHiddenLayer;i++){
			network.addLayer(new BasicLayer(new ActivationSigmoid(), true, numberOfNeuronsInHiddenLayer));
		}
		network.addLayer(new BasicLayer(new ActivationSigmoid(), false,Constants.NUMBER_OF_CELLS_IN_GRID_POWER));
		network.getStructure().finalizeStructure();
		network.reset();
		
		// create training data
		trainingSet = new BasicMLDataSet(INPUT, OUTPUT);

		// train the neural network
		final ResilientPropagation train = new ResilientPropagation(network,trainingSet);
		int epoch = 1;
		Static.log("Zacinam ucit neuronovou sit pro patro: "+floor);
		do {
			train.iteration();
			Static.log("Epoch #" + epoch + " Error:" + train.getError());
			Static.saveDataForNeuralToFile("Epoch #" + epoch + " Error:" + train.getError()+"\n");
			epoch++;
			
			
			if (epoch > MAX_EPOCH){
				Static.log("Prekrocen max pocet epoch");
				Static.saveDataForNeuralToFile("Prekrocen max pocet epoch\n");
				break;
			}
			
		} while (train.getError() > NEURAL_ERROR);
		train.finishTraining();
		
		Static.log("Ukladam neuronovou sit do souboru");
		EncogDirectoryPersistence.saveObject(getFileForNeural(floor), network);
		saveMd5ForFloor(floor, getMd5OfFile(getFileDataGrid(floor)));
		
		
		Static.saveDataForNeuralToFile("Test: "+network.calculateError(trainingSet));
		/*
		Static.saveDataForNeuralToFile("Test pro trenovaci data: \n");
		for(MLDataPair data : trainingSet) {
			MLData result = network.compute(data.getInput());
			saveDataForNeuralToFile(new double[][] { data.getInput().getData() }, new double[][] {result.getData()}, false);
		}
		*/
		// Encog.getInstance().shutdown();
		
	}
	
	public void test(){
		
		/*
		Static.deleteDataForNeuralFromFile();
		
		// EMPTY
		Static.saveDataForNeuralToFile("Empty: \n");
		double[] networkResult = new double[Constants.NUMBER_OF_CELLS_IN_GRID_POWER];
		
		network.compute(getEmptyInput(), networkResult);
		for(int i=0;i<Constants.NUMBER_OF_CELLS_IN_GRID_POWER;i++){
			Static.saveDataForNeuralToFile(networkResult[i]+" \n");
		}
		
		Static.saveDataForNeuralToFile("Suqare: 17:15 \n");
		ScanResult scanResult = new ScanResult();
		scanResult.addForceNoSort(new ScanResultAp("eduroam", "00:12:da:e5:1d:24",-51));
		scanResult.addForceNoSort(new ScanResultAp("eduroam", "00:12:da:e5:1d:23",-51));
		scanResult.addForceNoSort(new ScanResultAp("eduroam", "00:12:da:e5:16:72",-53));
		scanResult.addForceNoSort(new ScanResultAp("eduroam", "00:12:da:e5:16:94",-61));
		scanResult.addForceNoSort(new ScanResultAp("eduroam", "00:12:da:e5:16:93",-62));
		scanResult.addForceNoSort(new ScanResultAp("eduroam", "00:12:da:e5:1a:a2",-69));
		scanResult.addForceNoSort(new ScanResultAp("eduroam", "00:12:a2:84:2b:54",-72));
		scanResult.addForceNoSort(new ScanResultAp("eduroam", "00:12:a2:84:2b:53",-75));
		
		network.compute(convertScanResulForNeural(scanResult), networkResult);
		for(int i=0;i<Constants.NUMBER_OF_CELLS_IN_GRID_POWER;i++){
			Static.saveDataForNeuralToFile(networkResult[i]+" \n");
		}
		
		*/
		
		
	}
	
	// find X,Y
	public FindResutlHolder find(ScanResult dataFromScan){
		
		//FindResutlHolder results = null;
		int[][] fitnessGrid = FindPosition.createFitnessGrid();
		
		if(dataFromScan.getScanResultsCount() < 0){
			Static.log("Neuronova sit nedostala na vstup zadne data");
		}
		
		double[] scannedData = convertScanResulForNeural(dataFromScan);
		//Static.saveDataForNeuralToFile(getStringOfNeuronInput(scannedData));
		//Static.log(getStringOfNeuronInput(scannedData));
		
		
		
		// compute for scanned data
		double[] networkResult = new double[Constants.NUMBER_OF_CELLS_IN_GRID_POWER];

		
		network.compute(scannedData, networkResult);
		
		//Static.saveDataForNeuralToFile("Mereni: \n");
		//saveDataForNeuralToFile(new double[][] {scannedData},new double[][] { networkResult }, false);
				
		double[][] networkResultGrid = convertNeuralXYtoArray(networkResult);
		
	
		for(int x = 0; x < SettingsManager.GRID_SIZE; x++) {
			for(int y = 0; y < SettingsManager.GRID_SIZE; y++) {
				double networkValue = networkResultGrid[x][y];
				
				if(networkValue > 1){
					networkValue = 1;
				}
				if(networkValue <= NEURAL_ERROR*multiplyConstant) {
					networkValue = 0;
				}
				
				double networkValueMultiply = networkValue*multiplyConstant;
				
				
				fitnessGrid[x][y] = (int) Math.round( (multiplyConstant - networkValueMultiply));
				
				if( fitnessGrid[x][y] == multiplyConstant ){
					fitnessGrid[x][y] = Integer.MAX_VALUE;
				}
				else {
					Static.log("neuronova sit vratila vysledek");
				}
								
				
			}
		}
		

		FindResutlHolder results = new FindResutlHolder();
		results.setFloor(floor);
		results.addFindResults(fitnessGrid);
		Static.log("Neuronova sit urcila: "+results.getBestFitness().size());
		
		return results;
		
	}
	
	
	private double[][] convertListOfNeuralDataToInput(List<NeuralData> dataList){
		double[][] result = new double[dataList.size()][allBssid.size()];
		int i=0;
		for(NeuralData data : dataList){
			result[i] = data.getInput(); 
			i++;
		}
		return result;
	}
	
	private double[][] convertListOfNeuralDataToOutput(List<NeuralData> dataList){
		double[][] result = new double[dataList.size()][Constants.NUMBER_OF_CELLS_IN_GRID_POWER];
		int i=0;
		for(NeuralData data : dataList){
			double[] xy = data.getOutput();
			result[i] = convertXYForNeural((int)xy[0],(int)xy[1]); 
			i++;
		}
		return result;
	}
	
	private double[] convertXYForNeural(int x, int y) {
		double[][] result = getEmptyOutputTwoDimensional();
		
		result[x][y] = 1.0;
		
		double[] finalResult = getEmptyOutput();
		
		int count = 0;
		for(int x1=0;x1<SettingsManager.GRID_SIZE;x1++){
			for(int y1=0;y1<SettingsManager.GRID_SIZE;y1++){
				double value = result[x1][y1];
				finalResult[count] = value;
				count++;
			}
		}
		
		return finalResult;
	}
	
	
	private double[][] convertNeuralXYtoArray(double[] neuralResult) {
		double[][] result = getEmptyOutputTwoDimensional();
		
		int count = 0;
		for(int x=0;x<SettingsManager.GRID_SIZE;x++){
			for(int y=0;y<SettingsManager.GRID_SIZE;y++){
				result[x][y] = neuralResult[count];
				count++;
			}
		}
		
		return result;
	}
	
		
	public double[] convertScanResulForNeural(ScanResult scanResults) {
		Integer[] result = new Integer[allBssid.size()];

		// init
		HashMap<String, Integer> map = new HashMap<String, Integer>();
		for (int i = 0; i < allBssid.size(); i++) {
			map.put(allBssid.get(i), (int)NO_AP_NEURAL_INIT_VALUE);
		}

		// add values from scanResult
		for (ScanResultAp ap : scanResults.getListResult()) {
			if (map.containsKey(ap.getBssid())) {
				int level = Math.abs(ap.getLevel());
				if (level > 100) {
					level = 100;
				}
				level = 100 - level;
				map.put(ap.getBssid(), level);
			}
		}

		// put sorted values to int array
		int i = 0;
		SortedSet<String> keys = new TreeSet<String>(map.keySet());
		for (String key : keys) {
			result[i] = map.get(key);
			i++;
		}

		// convert Integer[] to double[]
		double[] resultDouble = new double[allBssid.size()];
		for (i = 0; i < allBssid.size(); i++) {
			resultDouble[i] =  ((double) result[i]) / 100.0 ;
		}

		return resultDouble;
	}
	
	
	
	// EMPTY ARRAYS
	private double[] getEmptyInput(){
		double[] result = new double[allBssid.size()];	
		Arrays.fill(result,NO_AP_NEURAL_INIT_VALUE);
		return result;
	}
	
	
	private double[] getEmptyOutput(){
		double[] result = new double[Constants.NUMBER_OF_CELLS_IN_GRID_POWER];
		Arrays.fill(result,0);
		return result;
	}
	
	private double[][] getEmptyOutputTwoDimensional(){
		double[][] result = new double[SettingsManager.GRID_SIZE][SettingsManager.GRID_SIZE];
		for(int x = 0; x < SettingsManager.GRID_SIZE; x++) {
			for(int y = 0; y < SettingsManager.GRID_SIZE; y++) {
				result[x][y] = 0;
			}
		}
		return result;
	}
	
	// FILES FOR NEURAL
	// file for save
	
	public static File getFileDataGrid(int floor) {
		File sdCard = Environment.getExternalStorageDirectory();
		File dir = new File (sdCard.getAbsolutePath() + Constants.XML_DATA_DIR);
		dir.mkdirs();
		File file = new File(dir, Constants.XML_DATA_FILENAME+floor+Constants.XML_DATA_SUFFIX);
		return file;
	}
	
	public static File getFileForNeural(int floor) {
		File sdCard = Environment.getExternalStorageDirectory();
		File dir = new File (sdCard.getAbsolutePath() + Constants.XML_DATA_DIR + "/" + Constants.XML_NEURAL_DATA_DIR);
		dir.mkdirs();
		File file = new File(dir, Constants.XML_DATA_FILENAME+floor+Constants.XML_NEURAL_DATA_SUFFIX);
		return file;
	}
	
	public static String getMd5OfFile(File file){
		String signature = "NO_DATA";
		try {
			if (file.exists()) {
				 FileInputStream fin = new FileInputStream(file);
				 String fileString = Static.convertStreamToString(fin);
				 fin.close();
				
				 MessageDigest md5 = MessageDigest.getInstance("MD5");
				 md5.update(fileString.getBytes(),0,fileString.length());
				 signature = new BigInteger(1,md5.digest()).toString(16);
			}
		}
		catch (Exception e){
			Static.logExceptionDetail(e);
		}
		return signature;
	}
	
	public void saveMd5ForFloor(int floor,String md5){
		SharedPreferences sharedPref = context.getSharedPreferences("md5files", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString("md5"+floor, md5);
		editor.commit();
	}
	
	public String getSavedMd5OfForFloor(int floor){
		SharedPreferences sharedPref = context.getSharedPreferences("md5files", Context.MODE_PRIVATE);
		String md5 = sharedPref.getString("md5"+floor, "");
		return md5;
	}
	
	
	/* OLD CODE
	private void saveDataForNeuralToFile(double[][] INPUT, double[][] OUTPUT,Boolean train) {
		int max;
		if (train) {
			max = INPUT.length; 
		}
		else {
			max = 1;
		}
		String data="";
		for(int i=0;i<max;i++){
			if (train){
				data += "\nTRAIN: \n";
			}
			else {
				data += "\nTEST: \n";
			}
			data += "\nINPUT: \n";
			data += getStringOfNeuronInput(INPUT[i]);
			data += "\nOUTPUT: \n";
			data += getStringOfNeuronOutput(OUTPUT[i]);
		}
		
		Static.saveDataForNeuralToFile(data);
	}

	
	
	
	private String getStringOfNeuronOutput(double[] result){
		double[][] network = convertNeuralXYtoArray(result);
		String data = "";
		
		for(int x=0;x<SettingsManager.GRID_SIZE;x++){
			for(int y=0;y<SettingsManager.GRID_SIZE;y++){
				data += network[x][y] + " ";
			}
			data += "\n";
		}
		
		return data;
	}
	
	private String getStringOfNeuronInput(double[] input){
		String data = "";
		for(int i=0;i<allBssid.size();i++){
			data += input[i]+"\n";
		}
		return data;
	}
	
	private int[][] joinFitnessGrids(List<int[][]> fitnessGridHolder){
		int[][] fitnessGridCount = FindPosition.createFitnessGridFillWithZeros();
		
		for (int[][] fitnessGrid : fitnessGridHolder){
			for(int x=0;x<SettingsManager.GRID_SIZE;x++){
				for(int y=0;y<SettingsManager.GRID_SIZE;y++){
					if(fitnessGrid[x][y] != 0){
						fitnessGridCount[x][y] += 1; 
					}
				}
			}
		}
		
		int[][] fitnessGridJoin = FindPosition.createFitnessGrid();
		
		for(int x=0;x<SettingsManager.GRID_SIZE;x++){
			for(int y=0;y<SettingsManager.GRID_SIZE;y++){
				if (fitnessGridCount[x][y] > (NUM_OF_FITNESSGRID_TO_COMPUTE-1)){
					// test if there is data for this field
					if(scanResultsGrid.getScanResultGrid()[x][y] != null) {
						fitnessGridJoin[x][y] = 0;
						for (int[][] fitnessGrid : fitnessGridHolder){
							fitnessGridJoin[x][y] += fitnessGrid[x][y];
						}
					}
				}
			}
		}
		
		return fitnessGridJoin;
		
	}
	
		*/
	
	
	

}
