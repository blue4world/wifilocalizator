package cz.mendelu.wifilocalizator.neuralnetwork;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import cz.mendelu.wifilocalizator.other.Static;

public class NeuralDataHolder {
	List<NeuralData> neuralDataList = new ArrayList<NeuralData>();
	
	public void add(NeuralData neuralData){
		//double[] input = neuralData.getInput();
		
		//if (!isPresentInList(input)){
			neuralDataList.add(neuralData);
		//}
		
	}
	
	public int getSize(){
		return neuralDataList.size();
	}
	
	public void removeDuplicateData(){
		List<NeuralData> newNeuralDataList = new ArrayList<NeuralData>();
		for(NeuralData neuralData : neuralDataList) {
			Boolean found = false;
			for(NeuralData neuralData2 : newNeuralDataList){
				if (Arrays.equals(neuralData.getInput(), neuralData2.getInput())){
					found = true;
					break;
				}
			}
			if (found == false){
				newNeuralDataList.add(neuralData);
			}
		}
		neuralDataList = newNeuralDataList;
	}
	
		
	public Boolean isPresentInList(double[] input){
		Boolean found = false;
		for (NeuralData nd : neuralDataList){
			double[] inputFromHolder = nd.getInput();
			Boolean isSame = isArraysSame(inputFromHolder, input);
			if (isSame){
				found = true;
				break;
			}
		}
		return found;
	}
	
	public Boolean isArraysSame(double[] array1,double[] array2){
		Boolean isSame = true;
		for(int i=0;i<array1.length;i++){
			if (array1[i] != array2[i]){
				isSame = false;
				break;
			}
		}
		return isSame;
	}
	
	public List<NeuralData> getNeuralData(){
		return neuralDataList;
	}
	
	/*
	public double[] editScannedDataForNeural(double[] inputData){
		//if (isPresentInList(inputData)){
		//	return inputData;
		//}
		//else {
			int[] fintess = new int[neuralDataList.size()];
			Arrays.fill(fintess, 0);
			
			for(int i=0;i<neuralDataList.size();i++){
				double[] inputFromHolder = neuralDataList.get(i).getInput();
				
				int maxPos = inputData.length;
				if(inputFromHolder.length < inputData.length){
					maxPos = inputFromHolder.length;
				}
				
				Boolean hasRun = false;
				for(int j=0;j<maxPos;j++){
					hasRun = true;
					fintess[i] += (int) Math.abs(inputData[j]-inputFromHolder[j]);
				}
				
				if (!hasRun) {
					fintess[i] = Integer.MAX_VALUE;
				}
				
			}
			
			// find min pos
			int min = Integer.MAX_VALUE;
			int pos = 0;
			for(int i=0;i<neuralDataList.size();i++){
				if (fintess[i] < min){
					min = fintess[i];
					pos = i;
				}
			}
			
			return neuralDataList.get(pos).getInput();
			
		//}
	}
	*/

}
