package cz.mendelu.wifilocalizator.neuralnetwork;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import cz.mendelu.wifilocalizator.dataholers.FindResutlHolder;
import cz.mendelu.wifilocalizator.dataholers.ScanResult;
import cz.mendelu.wifilocalizator.dataholers.ScanResultsFloors;
import cz.mendelu.wifilocalizator.dataholers.ScanResultsGrid;
import cz.mendelu.wifilocalizator.wififinder.FindPosition;

public class NeuralNetworkHolder {
	List<NeuralNetworkGripPowerOutput> networks = new ArrayList<NeuralNetworkGripPowerOutput>();
	Context context;
	
	public NeuralNetworkHolder(Context context) {
		this.context = context;
	}
	
	public void train(ScanResultsFloors scanResultsFloors){
		for (ScanResultsGrid scanResultsGrid : scanResultsFloors.getAllScanResultsGrids()){
			networks.add(new NeuralNetworkGripPowerOutput(scanResultsGrid, scanResultsGrid.getFloor(),context));
			networks.get(networks.size()-1).test();
		}
	}
	
	public FindResutlHolder getBestResultForData(ScanResult dataFromScan) {
		List<FindResutlHolder> findResutlHolders = new ArrayList<FindResutlHolder>();
		
		for (NeuralNetworkGripPowerOutput network : networks){
			FindResutlHolder findResutlHolder = network.find(dataFromScan);
			if (findResutlHolder != null) {
				findResutlHolders.add(findResutlHolder);
			}
		}
		
		if (findResutlHolders.size() > 0) {
			return FindPosition.getBestFindResultHolder(findResutlHolders);
		}
		else {
			return null;
		}
		
	}
	

}
