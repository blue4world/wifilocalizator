package cz.mendelu.wifilocalizator.neuralnetwork;

public class NeuralData {
	double[] input;
	double[] output;
	
	public NeuralData(double[] input,double[] output) {
		this.input = input;
		this.output = output;
	}
	
	public double[] getInput(){
		return input;
	}
	
	public double[] getOutput(){
		return output;
	}

}
