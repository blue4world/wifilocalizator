package cz.mendelu.wifilocalizator.neuralnetwork;

import java.util.ArrayList;
import java.util.List;

import cz.mendelu.wifilocalizator.dataholers.FindResutlHolder;
import cz.mendelu.wifilocalizator.dataholers.ScanResult;
import cz.mendelu.wifilocalizator.dataholers.ScanResultsFloors;
import cz.mendelu.wifilocalizator.dataholers.ScanResultsGrid;
import cz.mendelu.wifilocalizator.wififinder.FindPosition;

public class HopdieldNeuralNetworkHolder {
	List<HopfieldNeuralNetwork> networks = new ArrayList<HopfieldNeuralNetwork>();
	
	public HopdieldNeuralNetworkHolder() {
	}
	
	public void train(ScanResultsFloors scanResultsFloors){
		for (ScanResultsGrid scanResultsGrid : scanResultsFloors.getAllScanResultsGrids()){
			networks.add(new HopfieldNeuralNetwork(scanResultsGrid, scanResultsGrid.getFloor()));
		}
	}
	
	public FindResutlHolder getBestResultForData(ScanResult dataFromScan) {
		List<FindResutlHolder> findResutlHolders = new ArrayList<FindResutlHolder>();
		
		for (HopfieldNeuralNetwork network : networks){
			findResutlHolders.add(network.find(dataFromScan));
		}
		
		return FindPosition.getBestFindResultHolder(findResutlHolders);
		
	}
	

}
