package cz.mendelu.wifilocalizator;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import com.qozix.mapview.MapView;
import cz.mendelu.wifilocalizator.dataholers.FindResutlHolder;
import cz.mendelu.wifilocalizator.dataholers.TestHolder;
import cz.mendelu.wifilocalizator.interfaces.MapUpdated;
import cz.mendelu.wifilocalizator.managers.ImageLocationManager;
import cz.mendelu.wifilocalizator.other.Constants;
import cz.mendelu.wifilocalizator.other.MapHelper;
import cz.mendelu.wifilocalizator.other.Static;
import cz.mendelu.wifilocalizator.wifiscan.WifiScanLocalizerTimer;

public class LocalizeActivity extends DefaultMapActivity implements MapUpdated{
	LocalizeActivityHandler handler = new LocalizeActivityHandler();
	ImageLocationManager locationManager;
	LocalizeActivity activity;
	Boolean resultTracking = false;
	Boolean isTestRunning = false;
	public TestHolder testHolder = null;
	Boolean tapEnable = false;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		try {
			DefaultMapActivity.setChildActivity(this);
			activity = this;
			super.onCreate(savedInstanceState);
			
			this.locationManager = new ImageLocationManager(this);
			setUpMapEventListener();
		}
		catch (Exception e){
			Static.logExceptionDetail(e);
		}
	}
	
	   public class LocalizeActivityHandler extends Handler{
	    	public static final String dataTextSignal = "dataTextSignal";
	    	public static final String dataTextLastTime = "dataTextLastTime";
	    	public Boolean disableHandleMessage = false;
	    	
	    	public void disableHandleMessage(){
	    		disableHandleMessage = true;
	    	}
	    	
	    	public void enableHandleMessage(){
	    		disableHandleMessage = false;
	    	}
	    	
	    	private int getLocationFloorNumber(Message msg){
	    		return msg.getData().getInt(Constants.MSG_floor);
	    	}
	    	
	    	@Override
	    	public void handleMessage(Message msg) {
	    		if (!disableHandleMessage) {
		    		// update map
		    		int floor = getLocationFloorNumber(msg);
		    		if (floor != MapHelper.currentFloor && !ImageLocationManager.FIND_POSITION_SHOW_ALL_FOUND_SQUARES && resultTracking) {
		    			activity.selectItem(floor);
		    		}
	    			// handle msg
		    		locationManager.handleMsg(msg);
		    		
		    		
		    		if (resultTracking && !ImageLocationManager.FIND_POSITION_SHOW_ALL_FOUND_SQUARES) {
			    		try {
			    			FindResutlHolder findResutlHolder = locationManager.getFindResultHolder();
			    			if (! findResutlHolder.isEmpty() ) {
					    			int[] gridPosition = FindResutlHolder.getPositionOFBestFintessValue(findResutlHolder.getBestFitness());
					    			int[] absolutePosition = MapHelper.gridPositionToTrimPosition(gridPosition);
					    			MapHelper.mapView.moveToAndCenter(absolutePosition[0], absolutePosition[1], true);
			    			}
			    		}
			    		catch (Exception e){
			    			Static.logExceptionDetail(e);
			    		}
		    		}
		    		
		    		
		    		super.handleMessage(msg);
	    		}
	    		
	    	}

	    }
	   
	   
	@Override
	protected void onStop() {
		handler.disableHandleMessage();
		WifiScanLocalizerTimer.stop();
		locationManager.cleanUp();
		super.onStop();
	}
	
	@Override
	public void onResume() {
		Static.log("OnResume");
		handler.enableHandleMessage();
		try {
			new WifiScanLocalizerTimer(activity, scanResultsFloors, handler).start(); 
		} catch (Exception e) {
			Static.logExceptionDetail(e);
		}
		super.onResume();
	}
	
	@Override
	public void mapUpdated(){
		setUpMapEventListener();
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.localize, menu);
	    return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(item.getItemId() == R.id.tracking) {
			if (resultTracking) {
				resultTracking = false;
				item.setIcon(R.drawable.ic_action_location_off);
			}
			else {
				resultTracking = true;
				item.setIcon(R.drawable.ic_track_on);
			}
		}
		if (item.getItemId() == R.id.switch_to_scan) {
			Static.goToScanActivity(this);
		}
		
		if (item.getItemId() == R.id.test) {
			if(isTestRunning){
				isTestRunning = false;
				// stop test
				item.setTitle(R.string.test_start);
				if (testHolder != null){
					testHolder.save();
				}
				testHolder = null;
				Static.showToast(activity, getString(R.string.saving_to_file));
			}
			else {
				// start test
				isTestRunning = true;
				item.setTitle(R.string.test_end);
				tapEnable = true;
				Static.showToast(this, getString(R.string.choose_test_location));
			}
		}
		
		if (item.getItemId() == R.id.show_all_results) {
			if (ImageLocationManager.FIND_POSITION_SHOW_ALL_FOUND_SQUARES == true) {
				ImageLocationManager.FIND_POSITION_SHOW_ALL_FOUND_SQUARES = false;
				item.setIcon(R.drawable.ic_action_location_place);
				Static.showToast(this, getString(R.string.showing_best_result));
			}
			else {
				ImageLocationManager.FIND_POSITION_SHOW_ALL_FOUND_SQUARES = true;
				item.setIcon(R.drawable.ic_action_location_places);
				Static.showToast(this, getString(R.string.showing_all_results));
			}

		}
		return super.onOptionsItemSelected(item);
	}
	
	
	private void setUpMapEventListener(){
			MapHelper.mapView.addMapEventListener(new MapView.MapEventListener() {
			
			@Override
			public void onZoomStart(double scale) {
				
			}
			
			@Override
			public void onZoomLevelChanged(int oldZoom, int currentZoom) {
				
			}
			
			@Override
			public void onZoomComplete(double scale) {
				
			}
			
			@Override
			public void onTap(int x, int y) {
				if (tapEnable) {
					tapEnable = false;
					int[] absolutePosition = MapHelper.relativePositionToAbsolute(x, y);
					int[] trimmedPosition = MapHelper.trimPositionToGrid(absolutePosition);
					int[] coordinates = MapHelper.getGridCoordinates(trimmedPosition);
					testHolder = new TestHolder();
					testHolder.reset(coordinates[0], coordinates[1]);
					Static.dialogWithText(activity, getString(R.string.choosed_test_location_coordinates)+"\nX: "+coordinates[0]+" Y: "+coordinates[1]);
				}
			}
			
			@Override
			public void onScrollChanged(int x, int y) {
				
			}
			
			@Override
			public void onScaleChanged(double scale) {
				//Static.log("onScaleChanges: "+scale);
				locationManager.onScaleChanged(scale);
			}
			
			@Override
			public void onRenderStart() {
				
			}
			
			@Override
			public void onRenderComplete() {
				
			}
			
			@Override
			public void onPinchStart(int x, int y) {
				
			}
			
			@Override
			public void onPinchComplete(int x, int y) {
				
			}
			
			@Override
			public void onPinch(int x, int y) {
				
			}
			
			@Override
			public void onFlingComplete(int x, int y) {
				
			}
			
			@Override
			public void onFling(int sx, int sy, int dx, int dy) {
				
			}
			
			@Override
			public void onFingerUp(int x, int y) {
				
			}
			
			@Override
			public void onFingerDown(int x, int y) {
				
			}
			
			@Override
			public void onDrag(int x, int y) {
				
			}
			
			@Override
			public void onDoubleTap(int x, int y) {
				
			}
		});
	}

}
