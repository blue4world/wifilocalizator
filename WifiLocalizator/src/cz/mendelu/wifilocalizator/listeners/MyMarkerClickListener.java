package cz.mendelu.wifilocalizator.listeners;

import com.qozix.mapview.MapView;

import cz.mendelu.wifilocalizator.SampleCallout;
import android.view.View;
import android.widget.ImageView;

public class MyMarkerClickListener implements ImageView.OnClickListener{
	MapView mapView;
	
	public MyMarkerClickListener(MapView mapView) {
		this.mapView = mapView;
	}
	
	
	@Override
	public void onClick(View v) {
		// we saved the coordinate in the marker's tag
		double[] point = (double[]) v.getTag();
		// lets center the screen to that coordinate
		mapView.slideToAndCenter( point[0], point[1] );
		// create a simple callout
		SampleCallout callout = new SampleCallout( v.getContext() );
		// add it to the view tree at the same position and offset as the marker that invoked it
		
		//mapView.addCallout( callout, point[0], point[1], -0.5f, -1.0f );
		mapView.addCallout( callout, point[0], point[1], 0f, 0f );
		// a little sugar
		callout.transitionIn();
	}

}
